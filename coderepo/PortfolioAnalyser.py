import numpy as np
import pandas as pd
import datetime as dt
import scipy.optimize as sco
import empyrical as empy
import matplotlib.pyplot as plt
from GeneralUtilities import GeneralUtilities
from ModernPortfolioTheory import ModernPortfolioTheory
directory = 'C:\\Users\\43362080\\PortfolioAnalyser\\'
# General Utilities Object Instance
GU = GeneralUtilities()
# Modern PortfolioTheory Object Instance
MPT = ModernPortfolioTheory()
class PortfolioAnalyser(object):
    """description of class"""
    def Portfolio(self,tickers,start_date,rb_interval = 21, portType = None,option=None,weight=None,dataPath = 'Data/adjclose/',**kwargs):
        rd = [pd.read_csv(dataPath + '%s.csv' %t, index_col='Date').dropna(how='all') for t in tickers]
        
        for i in range(len(tickers)):
            try:
                rd[i].index = pd.to_datetime(rd[i].index, format='%d/%m/%Y')
            except ValueError:
                rd[i].index = pd.to_datetime(rd[i].index, format='%m/%d/%Y')
            rd[i] = rd[i]['Last Price'].sort_index()
        # Raw Data
        rawDataPrice = pd.concat(rd,axis=1).dropna()
        rawData = pd.concat(rd, axis=1).dropna().pct_change().dropna()
        #rawData = pd.concat(rd, axis=1).dropna()
        rawData.columns = tickers
        '''
        # Start & End Date Handler
        if type(start_date) == (dt.date or dt.datetime):
            if start_date < rawData.index[0].date():
                start_idx = 0
            else:
                start_idx = rawData.index.get_loc(start_date, method='bfill')
        '''
        start_idx = self.idxTarget(start_date,rawData)
        end_idx = rawData.shape[0]
        rb_date = rawData.index[np.arange(start_idx,end_idx, rb_interval)].tolist()
    
        # Mode Selection
        portName = str(portType)
        if portType == None:
            composition = pd.DataFrame(1/len(tickers), index=rb_date, columns=tickers)
            portName = 'Equal Weighted'
        elif portType == 'MPT':
        
            portName, composition = MPT.MPT(tickers, rawData,rb_date, option,**kwargs)
        else:
            raise ValueError('portType must be [MPT, None]')
        
        print(portName+' Done') 
        # Output
        # GU.outputFile('rawData.csv',rawData)
        # GU.outputFile('rb_date.csv',rb_date)
        # GU.outputFile('portName.csv',portName)
        # GU.outputFile('Composition.csv',composition)
        print('Output complete')
        # rawData & rb_date is generated from Portfolio function
        # portName & Composition is dependent on the method used
        return portName, composition, rawData, rb_date
    
    def idxTarget(self,target,dataFrame):
        # Returns index where the value can be found
        # String
        if type(target)== str:
            print('It is a string type')
            targetIdx = dataFrame.index.get_loc(target, method='bfill')
        # DateTime
        if type(target) == (dt.date or dt.datetime):
            if target < dataFrame.index[0].date():
                targetIdx = 0 # when date inputted is earlier than the entire range
            else:
                targetIdx = dataFrame.index.get_loc(target, method='bfill')
        return targetIdx
    def calc_Return(self, weight, ticker, date, leverage=None, rb_period=252):
        if leverage is None:
            leverage = pd.Series(1, index=ticker)
        else:
            leverage = pd.Series(leverage, index=ticker)
        weight = pd.Series(weight, index=ticker) * leverage
        date_idx = self.rawData.index.get_loc(date)
        sub_ret = self.rawData[ticker].iloc[date_idx-rb_period : date_idx]
        port_ret = sub_ret.dot(weight)
        annual_ret = empy.annual_return(port_ret)
        return annual_ret
    def sliceData(self, data, rb_date, rb_period):   
        #data = self.rawData
        date_idx = data.index.get_loc(rb_date)
        return_df = data.iloc[date_idx-rb_period:date_idx]
        return return_df
    def getAssetMetrics(self, data, rb_date, rb_period):
        return_df = self.sliceData(data, rb_date, rb_period)
        #print(return_df)
        x = empy.annual_return(return_df)
        y = empy.annual_volatility(return_df)
        metric = pd.DataFrame(x)
        metric.columns = ['Returns']
        metric['Volatility'] = y
        #metric.to_csv('metric' + str(rb_date)[:10] + '.csv')
        return metric
    def portfolio_annualised_performance(self, weights, mean_returns, cov_matrix):
            returns = np.sum(mean_returns*weights ) *252
            std = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights))) * np.sqrt(252)
            return std, returns
    def getFinalPortfolioReturn(self, data, composition, rb_date, rb_period):
        #======daily_ret should have the comp of the rb date============    
        daily_weight = composition.reindex(data.index).ffill().dropna()
        daily_ret = daily_weight.multiply(data.reindex(daily_weight.index)).sum(1)
        start_date = daily_ret.index[0].date()
        end_date = daily_ret.index[-1].date()
        return_df = self.sliceData(daily_ret, rb_date, rb_period)
        rawdata_df = self.sliceData(data, rb_date, rb_period)
        date_idx = composition.index.get_loc(rb_date)
        weight = composition.iloc[date_idx]
        std, returns = self.portfolio_annualised_performance(weight, rawdata_df.mean(), rawdata_df.cov())
        #std, returns = self.portfolio_annualised_performance(weight, rawdata_df.mean(), np.cov(rawdata_df))
        #std = empy.annual_volatility(return_df)
        #returns = empy.annual_return(return_df)
        #return_df.to_csv('fianlPortfolioReturn' + str(rb_date)[:10] + '.csv')
        return std, returns
    def backtest___(self, annual=True):    
        daily_weight = self.composition.reindex(self.rawData.index).ffill().dropna()
        daily_ret = daily_weight.multiply(self.rawData.reindex(daily_weight.index)).sum(1)
        start_date = daily_ret.index[0].date()
        end_date = daily_ret.index[-1].date()
        if annual:
            starts = [dt.date(y,1,1) for y in np.arange(start_date.year, end_date.year + 1)]
            ends = [dt.date(y,12,31) for y in np.arange(start_date.year, end_date.year + 1)]
        else:
            starts = [start_date]
            ends = [end_date]
        details={}
        for i in range(len(starts)):
            details.update({starts[i].year: [empy.annual_return(daily_ret[starts[i]:ends[i]]),
                                             empy.annual_volatility(daily_ret[starts[i]:ends[i]]),
                                            empy.sharpe_ratio(daily_ret[starts[i]:ends[i]]),
                                            empy.max_drawdown(daily_ret[starts[i]:ends[i]])]})
        details = pd.DataFrame(details).T
        details.columns=['Return','Volatility','Sharpe Ratio','Max. DD']
        cumprod = (daily_ret+1).cumprod()
        cumprod.plot()
        plt.savefig(directory + 'cumprod.png')
        plt.show()
        return (details, cumprod)
    def backtest(self, rawData, composition, rb_date, rb_interval,annual=True):
        daily_weight = composition.reindex(rawData.index).ffill().dropna()        
        daily_ret = daily_weight.copy() # Replication #daily_weight.multiply(self.rawData.reindex(daily_weight.index)).sum(1)
        #daily_ret = daily_weight.multiply(self.rawData.reindex(daily_weight.index))#.sum(1)
        # You need to copy
        # If you merely assign, DataFrame will be interconnected
        
        #daily_weight.to_csv(r'C:\Users\44128862\source\repos\CodeRepo\CodeRepo\DailyWeight_RAW.csv')
        rawData_Overall= rawData.reindex(daily_weight.index)+1 
        
        for i, v in daily_ret.iterrows():
            
            if i in rb_date :
                daily_ret.loc[i,:] = (rawData_Overall.loc[i,:]*daily_weight.loc[i,:])/sum(daily_weight.loc[i,:])
                daily_weight.loc[i,:]=rawData_Overall.loc[i,:]*daily_weight.loc[i,:]
                previousWeight=daily_weight.loc[i,:]
            else:
                #previousWeight = daily_weight.loc[i-dt.timedelta(days=1),:]
                daily_ret.loc[i,:] = (rawData_Overall.loc[i,:]*previousWeight)/sum(previousWeight)
                daily_weight.loc[i,:]=rawData_Overall.loc[i,:]*previousWeight
                previousWeight=daily_weight.loc[i,:]
    
        daily_ret=daily_ret.sum(1)-1
        print(daily_ret)
        daily_ret.to_csv('dailyReturn.csv')
        daily_weight.to_csv('dailyWeight.csv')
        rawData.to_csv('rawData.csv')
        composition.to_csv('composition.csv')
        #rawData_Overall.to_csv(r'C:\Users\44128862\source\repos\CodeRepo\CodeRepo\RawData.csv')
        #daily_weight.to_csv(r'C:\Users\44128862\source\repos\CodeRepo\CodeRepo\DailyWeight.csv')
        #daily_ret.to_csv(r'C:\Users\43362080\DailyRet.csv')
        #self.rawData.to_csv(r'C:\Users\43362080\rawData.csv')
        #daily_weight.to_csv(r'C:\Users\43362080\DailyRet.csv')
        #self.composition.to_csv(r'C:\Users\43362080\composition.csv')
        
        start_date = daily_ret.index[0].date() # First Element
        end_date = daily_ret.index[-1].date() # Last Element
        if annual:
            starts = [dt.date(y,1,1) for y in np.arange(start_date.year, end_date.year + 1)]
            ends = [dt.date(y,12,31) for y in np.arange(start_date.year, end_date.year + 1)]
        else:
            starts = [start_date]
            ends = [end_date]
        details={}
        for i in range(len(starts)):
            details.update({starts[i].year: [empy.annual_return(daily_ret[starts[i]:ends[i]]),
                                             empy.annual_volatility(daily_ret[starts[i]:ends[i]]),
                                            empy.sharpe_ratio(daily_ret[starts[i]:ends[i]]),
                                            empy.max_drawdown(daily_ret[starts[i]:ends[i]])]})
        details = pd.DataFrame(details).T
        details.columns=['Return','Volatility','Sharpe Ratio','Max. DD']
        cumprod = (daily_ret+1).cumprod()
        cumprod.plot()
        plt.savefig('cumprod.png')
        #plt.show()
        return (details, cumprod)
    def opt_result(self, rawData, composition, rb_date, rb_period):
        return_df = self.sliceData(rawData, rb_date, rb_period)
        date_idx = composition.index.get_loc(rb_date)
        weight = composition.iloc[date_idx]
        weight = weight.T
        daily_ret = return_df.mul(weight).sum(1)
        annual_return = empy.annual_return(daily_ret)
        annual_volatility = empy.annual_volatility(daily_ret)
        sharpe_ratio = empy.sharpe_ratio(daily_ret)
        max_drawdown = empy.max_drawdown(daily_ret)
        return annual_return, annual_volatility
    
    def random_portfolios(self, num_portfolios, mean_returns, cov_matrix, risk_free_rate): 
        results = np.zeros((3,num_portfolios))
        weights_record = []
        for i in range(num_portfolios):
            weights = np.random.random(3)          #TODO: need to remove the hard coded number of asset
            weights /= np.sum(weights)
            weights_record.append(weights)
            portfolio_std_dev, portfolio_return = self.portfolio_annualised_performance(weights, mean_returns, cov_matrix)
            results[0,i] = portfolio_std_dev
            results[1,i] = portfolio_return
            results[2,i] = (portfolio_return - risk_free_rate) / portfolio_std_dev
        return results, weights_record
        
    def portfolio_return(self, weights, mean_returns, cov_matrix):
        return self.portfolio_annualised_performance(weights, mean_returns, cov_matrix)[1]
    
    def efficient_return(self, mean_returns, cov_matrix, target, tickers, rb_date, leverage=None, rb_interval=21, rb_period=252):
        num_assets = len(mean_returns)
        args = (mean_returns, cov_matrix)
        constraints = ({'type': 'eq', 'fun': lambda x: self.portfolio_return(x, mean_returns, cov_matrix) - target},
                       {'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
        test_bound=([[0,0.4],
                     [0,0.2],
                     [0,0.2],
                     [0,0.2],
                     [0,0.2],
                     [0,0.4],
                     [0,0.4],
                     [0,0.15],
                     [0,0.2],
                     [0,0.1],
                     [0,0.2],
                     [0,0.2],
                     [0,0.05]])
        bounds = tuple((0,1) for asset in range(num_assets))
        #result = sco.minimize(volatility, num_assets*[1./num_assets,], args=args, method='SLSQP', bounds=bounds, constraints=constraints)
        fun = lambda x: -self.calc_Sharpe_Ratio(x, self.tickers, d, leverage=leverage.loc[d], rb_period=rb_period)
        result = sco.minimize(fun, num_assets*[1./num_assets,], args=args, method='SLSQP', bounds=bounds, constraints=constraints)
        return result
        
    def efficient_frontier(self, mean_returns, cov_matrix, returns_range, tickers, rb_date, leverage=None, rb_interval=21, rb_period=252):
        efficients = []
        for ret in returns_range:
            efficients.append(self.efficient_return(mean_returns, cov_matrix, ret, self.tickers, rb_date, leverage=None, rb_interval=21, rb_period=252))
        return efficients
    def visualize(self, data, composition, rb_date, rb_period, title, filename, ax=None, show_EF=True, option='Max Sharpe', **kwargs):
        return_df = self.sliceData(data, rb_date, rb_period)
        fig, ax = plt.subplots(figsize=(15, 15))
        ax.set_title(title)
        ran_pfs,_ = self.random_portfolios(50000, return_df.mean(), return_df.cov(), 0)
        plt.scatter(ran_pfs[0,:],ran_pfs[1,:],c=ran_pfs[2,:],cmap='YlGnBu', marker='o', s=10, alpha=0.3)
        metric = self.getAssetMetrics(data, rb_date, rb_period)
        ax = plt.scatter(metric['Volatility'], metric['Returns'], s=150, marker='s', color='b') # Needs to be dynamic
        bbox_dict = dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.4)
        for i in range(len(metric)):
            plt.annotate(metric.index[i], (metric.iloc[i]['Volatility'], metric.iloc[i]['Returns']), xytext=(-5, -20),textcoords='offset points', bbox=bbox_dict)        
        x, y = self.getFinalPortfolioReturn(data, composition, rb_date, rb_period)
        plt.scatter(x,y,marker='*',color='r',s=500, label=option)
        #=====issue with efficient frontier=====
        #target = [0,0.02,0.04,0.06,0.08,0.10,0.12,0.14,0.16,0.18,0.20]
        #efficient_portfolios = self.efficient_frontier(return_df.mean(), return_df.cov(), target, self.tickers, rb_date, leverage=None, rb_interval=21, rb_period=252)
        #plt.plot([p['fun'] for p in efficient_portfolios], target, linestyle='-.', color='black', label='efficient frontier')
        plt.legend(labelspacing=0.8)
        #plt.show()
        plt.savefig(filename, dpi=100)