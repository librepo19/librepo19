import numpy as np
import pandas as pd
import copy
import datetime as dt
import scipy.optimize as sco
import empyrical as empy
import matplotlib.pyplot as plt
from pypfopt.black_litterman import BlackLittermanModel
from GeneralUtilities import GeneralUtilities
from ModernPortfolioTheory import ModernPortfolioTheory

# General Utilities Object Instance
GU = GeneralUtilities()

# Modern PortfolioTheory Object Instance
MPT = ModernPortfolioTheory()

class PortfolioAnalyser_abcde(object):
    """description of class"""
    
    def Portfolio(self, tickers, Tbounds, group_pointer, groups, Gbounds, 
                  start_date, end_date, duration=None, time_horizon=None,  
                  black_litterman=False, view='absolute', tp_fn=None, leverage=1, lev_cost=0, 
                  annualization=252, rb_interval=21, rb_period=252, inputType='price', portType=None, option=None, 
                  dataPath='Data/adjclose/', dataFileName=None, dateFormat='%d-%m-%y', fill_method=None, 
                  directory=None, run_test=True, plot_graph_s='all', num_portfolios=50000, **kwargs):
        try:
            print(directory.split('\\')[-2])
        except IndexError:
            print(directory)
        
        portName = 'MPT'+' - '+option
        
        # check format seperate csv or not
        if dataFileName in [None, 'none', '', np.nan]:
            rd = [pd.read_csv(dataPath + '%s.csv' %t, index_col='Date')['Adj Close'].to_frame() for t in tickers]
        else:
            #rd = [pd.read_csv(dataPath + dataFileName + '.csv', index_col='Date').dropna(how='all')[t].to_frame() for t in tickers]
            rd = [pd.read_csv(dataPath + dataFileName + '.csv', index_col='Date')[t].to_frame() for t in tickers]
            
        for i in range(len(tickers)):
            rd[i].columns = [inputType] 
        for i in range(len(tickers)):
            try:
                rd[i].index = pd.to_datetime(rd[i].index, format=dateFormat)
            except ValueError:
                rd[i].index = pd.to_datetime(rd[i].index, format='%m/%d/%Y')
            rd[i] = rd[i][inputType].sort_index()
        
        # Raw Data
        rawData = pd.concat(rd, axis=1)
        
        def isnumber(x):
            try:
                float(x)
                return True
            except:
                return False

        rawData = rawData[rawData.applymap(isnumber)]
        rawData = rawData.applymap(float)
        rawData.columns = tickers
        
        if inputType == 'price':
            rawPrice = rawData
            rawReturn = rawPrice.pct_change()
        else:
            rawPrice = None
            rawReturn = rawData
            
        rawReturn.columns = tickers
        start_idx = self.idxTarget(start_date, rawReturn)
        
        if end_date == None:
            end_idx = rawReturn.shape[0]
        else:
            end_idx = self.idxTarget(end_date, rawReturn)
        
        rb_date = rawReturn.index[np.arange(start_idx, end_idx, rb_interval)].tolist()
        
        if time_horizon != None:
            if duration == None:
                DUR = pd.read_csv(dataPath + 'bonds_char.csv', index_col='ticker')['DUR_ADJ_MID']
                idxx = []
                for idx in DUR.index:
                    idxx.append(idx.replace('@',''))
                DUR.index = idxx
                # DUR = DUR.loc[~DUR.index.duplicated(keep='first')]
                # ^ remove repeated index (only useful for the fake bonds_char.csv)
                duration = DUR.reindex(tickers)
            else:
                duration = pd.Series(duration, index=tickers)

        ws = []
        fs = []
        figs_opt = []
        tic_dropped = []
        comp_pass = ['FAIL']*len(rb_date)
        plot_graph = False
        if plot_graph_s == 'all':
            plot_graph = True
        
        if view == 'mask00':
            prediction = pd.read_csv('AML_13views_as_prediction_fm.csv')
        elif view.startswith('prob') or view.startswith('old'):
            probability = pd.read_csv('probability_13ETF.csv')
        if tp_fn not in [None, 'none', '', np.nan]:
            target_price = pd.read_csv(dataPath+tp_fn, index_col='Date').ffill()
            target_price.index = pd.to_datetime(target_price.index, format=dateFormat)
            target_price = target_price.reindex(rawReturn.index).ffill()
            
        for index_date, date in enumerate(rb_date):
            d = str(date)[:10]
            print(d, end='\r')
            sub_ret = self.sliceData(rawReturn, date, rb_period, fill_method)
            # corrected return which subtracted the leverage cost
            cor_ret = sub_ret - (1 - 1/leverage)*lev_cost/annualization
            cor_ret_list = [cor_ret]
            tic_na_free = cor_ret.columns
            t_dropped = []
            for t in tickers:
                t_dropped.append(t not in tic_na_free)
            tic_dropped.append(t_dropped)
            
            if black_litterman and not view.startswith('old'):
                #if index_date==0:
                #    for t in tic_na_free:
                #        plt.figure()
                #        cor_ret[t].plot.hist(bins=20)
                #        plt.savefig(directory+'_'+d+'_'+t)
                #        plt.close()
                pi = cor_ret.mean()
                cov = cor_ret.cov()
                if view.startswith('absolute'):
                    # full abs view
                    P = np.eye(len(tic_na_free))
                    P = pd.DataFrame(P, index=tic_na_free) # index: view, columns: tic
                    if tp_fn in [None, 'none', '', np.nan]:
                        # god view
                        fut_ret = self.sliceData(rawReturn[tic_na_free], date, rb_interval, fill_method, False)
                        fut_ret = fut_ret - (1 - 1/leverage)*lev_cost/annualization
                        # Q0 = fut_ret.mean().T
                        #Q0 = ((1+fut_ret).cumprod().iloc[-1]-1).T/rb_interval
                        Q0 = ((1+fut_ret).cumprod().iloc[-1]-1)/rb_interval # rb return to line return
                    else:
                        # target price
                        #date_idx = target_price.index.get_loc(date)
                        #tp = target_price.iloc[date_idx+rb_interval]
                        tp = target_price.loc[date] # the prediction price after 12 month
                        cp = rawPrice.loc[date]
                        target_return = pd.Series(np.zeros(len(tic_na_free)), index=tic_na_free)
                        for t in tic_na_free:
                            target_return[t] = (tp[t]-cp[t])/cp[t]
                        fut_ret = target_return[tic_na_free]/annualization # annual return to line return
                        fut_ret = fut_ret.dropna()
                        Q0 = fut_ret - (1 - 1/leverage)*lev_cost/annualization
                        #Q0 = Q0.reshape(-1,1)
                    if view.endswith('01') or view.endswith('03') or view.endswith('04'):
                        # use formula 01, assume return is normal distributed
                        prob = pd.Series((np.sign(Q0)+1)/2, index=Q0.index)
                        vol = cor_ret.std()
                        Q = pd.Series(np.zeros(len(Q0)), index=Q0.index)
                        if view.endswith('03'):
                            k = np.sqrt(2/np.pi) # mean(X) given X>0, X~N(0,1)
                        elif view.endswith('04'):
                            k = 0.674 # median(X) given X>0, X~N(0,1)
                        else:
                            k = 1
                        for t in Q0.index:
                            Q[t] = vol[t]*2*(prob[t]-0.5)*k
                    elif view.endswith('02') or view.endswith('05'):
                        # use formula 02
                        prob = pd.Series((np.sign(Q0)+1)/2, index=Q0.index)
                        mean_pos = []
                        mean_neg = []
                        for t in Q0.index:
                            rs = cor_ret[t]
                            rs_pos = [r for r in rs if r>0]
                            mpos = sum(rs_pos)/len(rs_pos)
                            mean_pos.append(mpos)
                            rs_neg = [r for r in rs if r<0]
                            mneg = sum(rs_neg)/len(rs_neg)
                            mean_neg.append(mneg)
                        mean_pos = pd.Series(mean_pos, index=Q0.index)
                        mean_neg = pd.Series(mean_neg, index=Q0.index)
                        Q = pd.Series(np.zeros(len(Q0)), index=Q0.index)
                        for t in Q0.index:
                            if view.endswith('02'):
                                if prob[t]>=0.5:
                                    Q[t] = pi[t] + (mean_pos[t]-pi[t])*2*(prob[t]-0.5)
                                else:
                                    Q[t] = mean_neg[t] + (pi[t]-mean_neg[t])*2*prob[t]
                            else:
                                Q[t] = mean_neg[t] + (mean_pos[t]-mean_neg[t])*prob[t]
                    else:
                        Q = Q0
                    Q = Q.T     
                    P = P.loc[Q0.index]
                        
                elif view == 'relative':
                    # god relative view
                    fut_ret = self.sliceData(rawReturn[tic_na_free], date, rb_interval, fill_method, False)
                    fut_ret = fut_ret - (1 - 1/leverage)*lev_cost/annualization
                    Q_abs = ((1+fut_ret).cumprod().iloc[-1]-1).T/rb_interval
                    k = len(Q_abs)
                    K = k*(k-1)
                    P = np.zeros((K, len(tic_na_free)))
                    P = pd.DataFrame(P, columns=tic_na_free) # index: view, columns: tic
                    Q = np.zeros(K).reshape(-1,1)
                    i = 0
                    for j1, t1 in enumerate(fut_ret.columns):
                        for j2, t2 in enumerate(fut_ret.columns):
                            if t1!=t2:
                                P.iloc[i][t1] = 1
                                P.iloc[i][t2] = -1
                                Q[i] = Q_abs[j1]-Q_abs[j2]
                                i += 1
                elif view == 'mask00':
                    '''
                    Q:(pi,pred)->view
                    Q(-,-) = pi
                    Q(-,+) = 0
                    Q(+,-) = 0
                    Q(+,+) = pi
                    tested, seems not very useful
                    '''
                    pred = prediction.iloc[index_date][tic_na_free]
                    P = np.eye(len(tic_na_free))
                    P = pd.DataFrame(P, index=tic_na_free) # index: view, columns: tic
                    #print('pi')
                    #print(pi)
                    Q = pi.copy()
                    for t in tic_na_free:
                        if (Q[t]<0 and pred[t]==1) or (Q[t]>0 and pred[t]==0):
                            Q[t] = 0
                    Q = Q.reshape(-1,1)
                    #print('Q')
                    #print(Q)
                    
                elif view == 'prob01':
                    '''
                    '''
                    #print('pi')
                    #print(pi.T)
                    prob = probability.iloc[index_date*10:index_date*10+10][tic_na_free].mean()
                    #print('prob')
                    #print(prob.T)
                    P = np.eye(len(tic_na_free))
                    P = pd.DataFrame(P, index=tic_na_free) # index: view, columns: tic
                    vol = cor_ret.std()
                    Q = pd.Series(np.zeros(len(tic_na_free)), index=tic_na_free)
                    for t in tic_na_free:
                        k = 1
                        # k = np.sqrt(2/np.pi) # mean 0.797884
                        # k = 0.674 # median
                        # assume normal dist
                        Q[t] = vol[t]*(prob[t]-0.5)*2*k
                    # Q = Q.reshape(-1,1)
                    #print('vol')
                    #print(vol.T)
                    #print('Q')
                    #print(Q.T)
                elif view == 'prob02':
                    '''
                    '''
                    #print('pi')
                    #print(pi.values.T)
                    prob = probability.iloc[index_date*10:index_date*10+10][tic_na_free].mean()
                    #print('prob')
                    #print(prob.values.T)
                    P = np.eye(len(tic_na_free))
                    P = pd.DataFrame(P, index=tic_na_free) # index: view, columns: tic
                    Q = pd.Series(np.zeros(len(tic_na_free)), index=tic_na_free)
                    mean_pos = []
                    mean_neg = []
                    for t in tic_na_free:
                        rs = cor_ret[t]
                        rs_pos = [r for r in rs if r>0]
                        mpos = sum(rs_pos)/len(rs_pos)
                        mean_pos.append(mpos)
                        rs_neg = [r for r in rs if r<0]
                        mneg = sum(rs_neg)/len(rs_neg)
                        mean_neg.append(mneg)
                    mean_pos = pd.Series(mean_pos, index=tic_na_free)
                    mean_neg = pd.Series(mean_neg, index=tic_na_free)
                    for t in tic_na_free:
                        if prob[t]>=0.5:
                            Q[t] = pi[t] + (mean_pos[t]-pi[t])*2*(prob[t]-0.5)
                        else:
                            Q[t] = mean_neg[t] + (pi[t]-mean_neg[t])*2*prob[t]
                    Q = Q.reshape(-1,1)
                    #print('Q')
                    #print(Q.T)
                bl = BlackLittermanModel(cov, pi=pi, P=P.values, Q=Q)
                post_ret = bl.bl_returns()
                post_cov = bl.bl_cov()
                #print('post_ret')
                #print(post_ret.values.T)
                np.random.seed(0)
                num_ran_ret = 30 # <<<<<< 
                cor_ret_list = []
                for k in range(num_ran_ret):
                    ran = np.random.multivariate_normal(post_ret, post_cov, rb_period)
                    cor_ret = pd.DataFrame(ran, index=sub_ret.index, columns=sub_ret.columns)
                    #cor_ret -= cor_ret.mean()
                    #cor_ret += post_ret
                    cor_ret_list.append(cor_ret)
            
            elif black_litterman and view.startswith('old'):
                pi = cor_ret.mean()
                cov = cor_ret.cov()
                corr = cor_ret.corr()
                
                tau = .05  # scaling factor
                vols = cor_ret.std()
                
                names = tic_na_free
                
                
                prob = probability.iloc[index_date*10:index_date*10+10][tic_na_free].mean()
                #print('prob')
                #print(prob.T)
                views = []
                probs = []
                for i,t in enumerate(tic_na_free):
                    v = prob[i]*2-1
                    views.append([t,v])
                    probs.append([t,prob[i]])
                    
                if view.endswith('01'):
                    BL_options = 1
                else:
                    BL_options = 2
                view_strength = 1
                
                
                Q, P = self.prepare_views_and_link_matrix(names=names, views=views, vols=vols, 
                                                     prob=probs, BL_options=BL_options, 
                                                     view_strength=view_strength,
                                                     R=pi)
                R_bl = self.Pi_gen(tau, P, cov, cor_ret, Q, BL_options, corr, probs)
                #print(R_bl)
                
                R_bl = pd.DataFrame(R_bl.T, columns=names)
                
                
                cor_ret_list = [R_bl]
            
            
            
            
            w, f, As, lb, ub = MPT.MPT(cor_ret_list, tickers, tic_na_free, Tbounds, group_pointer, groups, Gbounds, 
                                       duration, time_horizon, option, leverage, lev_cost, annualization)
            ws.append(w)
            fs.append(f)
            con = {'As':As, 'lb':lb, 'ub':ub}
            
            if (not plot_graph) and (date==rb_date[-1]) and (plot_graph_s=='last'):
                plot_graph = True
            
            if (not run_test) and (not plot_graph):
                num_portfolios = 1
            
            info_opt, info_tic, info_tic_lev, info_ran, weights_ran \
                = self.getInfo(sub_ret, cor_ret_list[0], w[tic_na_free], leverage, annualization, con, num_portfolios)
            
            if plot_graph:
                fig = self.visualize(info_opt, info_tic, info_ran, tic_na_free, d, directory + d, option=option)
                figs_opt.append(fig)
            
            cell, comp_ast, comp_ran = self.info_table(info_opt, info_tic_lev, info_ran, tic_na_free, option, run_test)
            
            if run_test:
                if comp_ast and comp_ran:
                    # print('PASS')
                    comp_pass[index_date] = 'PASS'
                elif comp_ran:
                    # print('not better than a single asset')
                    comp_pass[index_date] = 'PASS_'
            
            GU.outputFile(directory + '%s_table.csv'%d, cell)
        
        weights = pd.DataFrame(ws, index=rb_date, columns=tickers)
        GU.outputFile(directory + 'composition.csv', weights)
        
        f_opt = pd.DataFrame(fs, index=rb_date, columns=[option])
        GU.outputFile(directory + 'func.csv', f_opt)
        
        tic_dropped = pd.DataFrame(tic_dropped, index=rb_date, columns=tickers)
        GU.outputFile(directory + 'tic_dropped.csv', tic_dropped)
        
        comp_pass = pd.DataFrame(comp_pass, index=rb_date, columns=['Test Result'])
        GU.outputFile(directory + 'comp_pass.csv', comp_pass)
        if run_test:
            cp = comp_pass['Test Result'].values.tolist()
            if all([c=='PASS' for c in cp]):
                print('All PASS    ')
            elif all([c[:4]=='PASS' for c in cp]):
                print('All PASS_   ') # check if the single asset is in bound
            else:
                print('NOT All PASS')

        # cut required slice of rawReturn for backtest
        rawReturn = rawReturn.iloc[start_idx: end_idx]
        if fill_method in ['ffill', 'bfill', 'pad', 'backfill']:
            rawReturn = rawReturn.fillna(method=fill_method)
        elif fill_method in ['mean', 'average', 'avg']:
            rawReturn = rawReturn.fillna(rawReturn.mean())
        rawReturn.to_csv(directory + 'rawReturn.csv')
        
        return portName, rawReturn, rb_date, weights, f_opt, figs_opt, tic_dropped, comp_pass
    
    def idxTarget(self, target, dataFrame):
        # Returns index where the value can be found
        # String
        if type(target) == str:
            #print('It is a string type')
            targetIdx = dataFrame.index.get_loc(target, method='bfill')
        # DateTime
        if type(target) == (dt.date or dt.datetime):
            if target < dataFrame.index[0].date():
                targetIdx = 0 # when date inputted is earlier than the entire range
            elif target > dataFrame.index[-1].date():
                targetIdx = dataFrame.shape[0] # when date inputted is later than the entire range
            else:
                targetIdx = dataFrame.index.get_loc(target, method='bfill')
        return targetIdx

    def sliceData(self, data, rb_date, num_lines, fill_method=None, find_backword=True):   
        date_idx = data.index.get_loc(rb_date)
        if find_backword:
            return_df = data.iloc[date_idx-num_lines:date_idx]
        else:
            return_df = data.iloc[date_idx:date_idx+num_lines]
        if fill_method in ['ffill', 'bfill', 'pad', 'backfill']:
            return_df = return_df.fillna(method=fill_method)
        elif fill_method in ['mean', 'average', 'avg']:
            return_df = return_df.fillna(return_df.mean())
        return_df = return_df.dropna(axis='columns')
        return return_df
    """
    # transforming random numbers to weights within constraints
    def random_weights(self, x, K, lb, ub, Ags, glbs, gubs):
        '''
        input random numbers [0,1) 
        output weights within constraints
        SUM w = K
        lb < w < ub
        glb < Ag @ w < gub
        '''
        if len(x) == 1:
            return np.array([K])
        loww = [max(lb[0], K-sum(ub[1:]))]
        uppp = [min(ub[0], K-sum(lb[1:]))]
        backsub = [True]*len(Ags)
        gs = []
        for idx_this, Ag_this in enumerate(Ags):
            Ag_this = Ags[idx_this]
            #print(Ag_this)
            g_this = Ag_this[:,0].tolist().index(1)
            gs.append(g_this)
            if sum(Ag_this[g_this]) != 1 and Ag_this.shape[0] > 1:
                for idx_that, Ag_that in enumerate(Ags):
                    if idx_that != idx_this:
                        for g_that, row in enumerate(Ag_that):
                            diff = Ag_this[g_this] - row
                            prod = Ag_this[g_this] * row
                            if any(diff > 0) and any(prod == 1): 
                                if all(diff >= 0):
                                    glbs[idx_that][g_that] \
                                        = max(glbs[idx_that][g_that], 
                                              K-sum(gubs[idx_that][:g_that])-sum(gubs[idx_that][g_that+1:])) 
                                    row_that = row
                                    gubs[idx_this][g_this] -= glbs[idx_that][g_that]
                                    glbs[idx_this] = np.append(glbs[idx_this], glbs[idx_that][g_that])
                                else:
                                    gubs[idx_that][g_that] \
                                        = min(gubs[idx_that][g_that], 
                                              K-sum(glbs[idx_that][:g_that])-sum(glbs[idx_that][g_that+1:])) 
                                    #row *= Ag_this[g_this]
                                    row_that = row * Ag_this[g_this]
                                    glbs[idx_this][g_this] -= gubs[idx_that][g_that]
                                    glbs[idx_this] = np.append(glbs[idx_this], 0)
                                Ag_this[g_this] -= row_that # same
                                Ag_this = np.vstack([Ag_this, row_that]) # same
                                gubs[idx_this] = np.append(gubs[idx_this], gubs[idx_that][g_that])
                                if Ag_this[g_this,0] != 1:
                                    g_this = Ag_this[:,0].tolist().index(1)
                                    gs[-1] = g_this
            uppp.append(min(gubs[idx_this][g_this], K-sum(glbs[idx_this][:g_this])-sum(glbs[idx_this][g_this+1:])))

            if sum(Ag_this[g_this]) == 1:
                loww.append(max(glbs[idx_this][g_this], K-sum(gubs[idx_this][:g_this])-sum(gubs[idx_this][g_this+1:])))
                Ag_this = np.delete(Ag_this, g_this, axis=0)
                glbs[idx_this] = np.delete(glbs[idx_this], g_this)
                gubs[idx_this] = np.delete(gubs[idx_this], g_this)
                backsub[idx_this] = False
            Ags[idx_this] = Ag_this

        low = max(loww)
        up = min(uppp)
        '''
        if(low>up):
            print('maybe some problem')
            print('lb')
            print(lb)
            print('ub')
            print(ub)
            print('Ags')
            print(Ags)
            print('glbs')
            print(glbs)
            print('gubs')
            print(gubs)
            print('loww')
            print(loww)
            print('uppp')
            print(uppp)
            print('low-up')
            print(low-up)
        '''
        x[0] = x[0]*(up-low) + low
        for gd, bs in enumerate(backsub):
            if bs:
                glbs[gd][gs[gd]] -= x[0]
                gubs[gd][gs[gd]] -= x[0]
        x[1:] = self.random_weights(x[1:], K-x[0], lb[1:], ub[1:], [Ag[:,1:] for Ag in Ags], glbs, gubs)
        return x
        """
    # transforming random numbers to weights within constraints
    def random_weights(self, x, K, lb, ub):
        '''
        SUM w = K
        lb < w < ub
        input random numbers [0,1)
        output weights within constraints
        '''
        
        if len(x) == 1:
            return np.array([K])
        
        # correct the lower bounds from upper bounds 
        # say a + b = K, b < uB  ---->  a > K - uB
        low = np.maximum(lb[0], K-sum(ub[1:]))
        
        # correct the upper bounds from lower bounds 
        # say a + b = K, b > lB  ---->  a < K - lB
        up = np.minimum(ub[0], K-sum(lb[1:]))
        
        x[0] = x[0]*(up-low) + low
        x[1:] = self.random_weights(x[1:], K-x[0], lb[1:], ub[1:])
        
        return x
    
    # redistribute wei to weights
    def distribute_weight(self, wei, weights, pointer):
        m = 0
        for k, bo in enumerate(pointer):
            if bo == 1:
                weights[k] = wei[m]
                m += 1
                if m == len(wei):
                    break
        return weights

    # generate random portfolios
    def random_portfolios(self, num_portfolios, return_df, rfr, annualization, leverage, con): 
        num_tickers = len(return_df.columns)
        As = con.get('As')
        lb = con.get('lb')
        ub = con.get('ub')
        
        pt = num_tickers
        tlb = np.array(lb[:pt], dtype=np.float64)
        tub = np.array(ub[:pt], dtype=np.float64)
        
        Ags = As[1:]
        glbs = []
        gubs = []
        for Ag in Ags:
            ng = Ag.shape[0]
            glbs.append(np.array(lb[pt:pt+ng], dtype=np.float64))
            gubs.append(np.array(ub[pt:pt+ng], dtype=np.float64))
            pt += ng

        results = []
        weights_record = []
        
        ntg = [int(sum(Ags[0][i,:])) for i in range(Ags[0].shape[0])]
        
        for i in range(num_portfolios):
            """  
            #weights = np.ones(num_tickers)*-1 # just initialize
            x = np.random.random(num_tickers)
            weights = self.random_weights(copy.deepcopy(x), leverage, tlb, tub, copy.deepcopy(Ags), copy.deepcopy(glbs), copy.deepcopy(gubs))
            A = np.vstack(Ags)
            A = np.vstack([np.eye(num_tickers), A])
            eps = 1.0e-9
            if not(all(A@weights >= lb-eps) and all(A@weights <= ub+eps)):
                '''
                print('i, x')
                print(i, x)
                print('weights')
                print(weights)
                print('A')
                print(A)
                print('A@weights')
                print(A@weights)
                print('A@weights >= lb')
                print(A@weights >= lb)
                print('A@weights <= ub')
                print(A@weights <= ub)
                '''
            else:    
                weights_record.append(weights)
                # the algorithm now is incorrect, many of the weights may be out of bound
            
            """
            weights = np.ones(num_tickers)*-1 # just initialize
            num_groups = Ags[0].shape[0]
            
            # genarate random group weights
            y = np.random.random(num_groups)
            weights_group = self.random_weights(y, leverage, glbs[0], gubs[0])
            
            # for every groups, genarate random weights for their assets
            for j in range(num_groups):
                x = np.random.random(ntg[j])
                wei = self.random_weights(x, weights_group[j], tlb, tub)
                weights = self.distribute_weight(wei, weights, Ags[0][j,:])
            weights_record.append(weights)
        
        weights_record = np.array(weights_record)
        #print('weights_record.shape')
        #print(weights_record.shape)
        results = self.getPortfolioInfo(return_df, rfr, weights_record, annualization)
            
        return results, weights_record
        
    # pure asset
    def getAssetInfo(self, return_df, rfr, annualization):
        std_dev = empy.annual_volatility(return_df, annualization=annualization) 
        sharpe_ratio = empy.sharpe_ratio(return_df, rfr, annualization=annualization) 
        expected_return = std_dev*sharpe_ratio + rfr
        result = []
        result.append(std_dev)
        result.append(expected_return)
        result.append(sharpe_ratio)
        return result

    # asset with given weight
    def getPortfolioInfo(self, return_df, rfr, weight, annualization):
        weighted_ret = return_df.dot(weight.T)
        #weighted_ret = return_df.mul(weight).sum(1)
        return self.getAssetInfo(weighted_ret, rfr, annualization)
    
    def getInfo(self, ret, ret_lc, weight_opt, leverage, annualization, constraints, num_portfolios = 50000):
                
        rfr = 0 # maybe input from config at later stage

        # random portfolio
        info_ran, weights_ran = self.random_portfolios(num_portfolios, ret_lc, rfr, annualization, leverage, constraints)
                
        # single ticker
        info_tic = self.getAssetInfo(ret, rfr, annualization)
        info_tic_lev = self.getAssetInfo(ret_lc*leverage, rfr, annualization)
        
        # optimized point
        info_opt = self.getPortfolioInfo(ret_lc, rfr, weight_opt, annualization)

        return info_opt, info_tic, info_tic_lev, info_ran, weights_ran

    def visualize(self, info_opt, info_tic, info_ran, tickers, title, filename, ax=None, option='Max Sharpe', **kwargs):
                
        rfr = 0 # maybe input from config at later stage
        
        plt.figure()
        fig, ax = plt.subplots(figsize=(15, 15))
        ax.set_title(title)
        ax.set_xlabel('Volatility')
        ax.set_ylabel('Expected Return')
        
        # random portfolio              
        [xs, ys, rs] = info_ran
        ax.scatter(xs, ys, c=rs, cmap='YlGnBu', marker='o', s=10, alpha=0.3)
        
        ann = []
        bbox_dict = dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.4)
        
        # single ticker
        [xs, ys, rs] = info_tic
        ax.scatter(xs, ys, s=150, marker='s', color='b', label='Asset') 
        for i, t in enumerate(tickers):
            [x,y,r] = [xs[i],ys[i],rs[i]]
            a = ax.annotate(t, (x,y), xytext=(-5, -20), textcoords='offset points', bbox=bbox_dict)
            ann.append(a)
        
        # optimized point
        [x, y, r] = info_opt
        ax.scatter(x, y, marker='*', color='r', s=500, label=option)
        a = ax.annotate('%s'%option, (x,y), xytext=(-5, 15), textcoords='offset points', bbox=bbox_dict) 
        ann.append(a)
        
        # check if tangent
        if option == 'Max Sharpe':
            ax.plot([0,x], [rfr,y])
        
        ax.legend(labelspacing=0.8, loc='upper left')
        
        #plt.show()
        plt.savefig(filename, dpi=100)
        
        for a in ann:
            a.remove()
        
        [xs, ys, rs] = info_tic
        for i, t in enumerate(tickers):
            [x,y,s] = [xs[i],ys[i],rs[i]]
            ax.annotate('%s (%.3f,%.3f,%.3f)'%(t, x, y, s), (x,y), 
                        xytext=(-5, -20), textcoords='offset points', bbox=bbox_dict)
        
        [x,y,s] = info_opt
        ax.annotate('%s (%.3f,%.3f,%.3f)'%(option, x, y, s), (x,y), 
                    xytext=(-5, 15), textcoords='offset points', bbox=bbox_dict)

        #plt.show()
        plt.savefig(filename + '_details', dpi=100)
        plt.close()
        
        return fig

    def info_table(self, info_opt, info_ast, info_ran, tickers, option, run_test):
        points = []
        cell_text = []
        comp_ast = True
        comp_ran = True
        
        [x_opt, y_opt, r_opt] = info_opt
        points.append(option)
        cell_text.append([x_opt, y_opt, r_opt])
        
        [xs_ast, ys_ast, rs_ast] = info_ast
        for i, t in enumerate(tickers):
            info = [xs_ast[i], ys_ast[i], rs_ast[i]]
            points.append(t)
            cell_text.append(info)
        
        [xs_ran, ys_ran, rs_ran] = info_ran
        
        if run_test:
            if option == 'Min Vol':
                idx = np.argmin(xs_ran)
                for x in xs_ast:
                    if x < x_opt:
                        comp_ast = False
                        break
                if xs_ran[idx] < x_opt:
                    comp_ran = False
            elif option == 'Max Sharpe':
                idx = np.argmax(rs_ran)
                for r in rs_ast:
                    if r > r_opt:
                        comp_ast = False
                        break
            if rs_ran[idx] > r_opt:
                comp_ran = False
            pt_name = option+'_from_ranpfs'
        else:
            idx = 0
            pt_name = 'ranpf'
        info = [xs_ran[idx], ys_ran[idx], rs_ran[idx]]
        points.append(pt_name)
        cell_text.append(info)
        
        result_df = pd.DataFrame(cell_text, columns=['Volatility', 'Return', 'Sharpe Ratio'])
        result_df['PT'] = points
        result_df = result_df.set_index('PT')
        
        return result_df, comp_ast, comp_ran 
    
    def opt_result(self, rawData, composition, rb_date, rb_period, annualization):
        return_df = self.sliceData(rawData, rb_date, rb_period)
        date_idx = composition.index.get_loc(rb_date)
        weight = composition.iloc[date_idx]
        weight = weight.T
        daily_ret = return_df.mul(weight).sum(1)
        annual_return = empy.annual_return(daily_ret,annualization=annualization) 
        annual_volatility = empy.annual_volatility(daily_ret,annualization=annualization) 
        sharpe_ratio = empy.sharpe_ratio(daily_ret,annualization=annualization) 
        max_drawdown = empy.max_drawdown(daily_ret)
        return annual_return, annual_volatility
    
    
    
    def prepare_views_and_link_matrix(self, names, views, vols, prob, R, BL_options=1, view_strength=1):
        """generate P,Q based on views:black-litterman"""    
        views = [x for x in views if x[0] in names]  # if views more than names, limit views range in names
        prob = [x for x in prob if x[0] in names]
        r, c = len(views), len(names)
        Q = np.zeros([r])
        P = np.zeros([r, c])  # link matrix
        nameToIndex = dict()
        for i, n in enumerate(names):
            nameToIndex[n] = i
        for i in range(r):
            if len(views[i]) == 4:  # if relative views
                Q[i] = views[i][3]
                name1, name2 = views[i][0], views[i][2]
                P[i, nameToIndex[name1]] = +1 if views[i][1] == '>' else -1
                P[i, nameToIndex[name2]] = -1 if views[i][1] == '>' else +1
            elif len(views[i]) == 2:  # if absolute views
                name1 = views[i][0]
                if (int(BL_options) == 1) or (BL_options is None):
                    # BL-A
                    if views[i][1] > 0:
                        Q[i] = vols[nameToIndex[name1]]*(1+prob[i][1])*view_strength
                    else:
                        Q[i] = vols[nameToIndex[name1]]*(2-prob[i][1])*view_strength
                elif int(BL_options) == 2:
                    # BL-B
                    Q[i] = vols[nameToIndex[name1]]*abs(prob[i][1]-0.5)*2*view_strength
                elif int(BL_options) == 3:
                    # Omega
                    Q[i] = R[i]*view_strength
                elif int(BL_options) == 4:
                    # Modified Omega
                    Q[i] = vols[nameToIndex[name1]]*view_strength 
                P[i, nameToIndex[name1]] = +1 if views[i][1] > 0 else -1
            else:
                raise ValueError('A wrong format of views!')
        return Q, P
    

    def Pi_gen(self, tau, P, C, R, Q, BL_options, corr, prob):
        """generate new return based on views: black-litterman
           tau: the scalar, between 0.01 and 0.05 
           P: a matrix that identifies the assets involved in the views (K x N matrix or
              1 x N row vector in the special case of 1 view);
           Q: the View Vector (K x 1 column vector).
           C: covariance of assets
           R: return of portfolio
           BL_options: Options for various Black-Litterman methods
           prob: Probability of "1" from AML predictions
           Return the return with black-Litterman views in"""
        # Calculate omega - uncertainty matrix about views
        # 0.025 * P * C * np.transpose(P)
        omega = np.dot(np.dot(np.dot(tau, P), C), np.transpose(P))
    
        if int(BL_options) >= 3:
           # For Omega & Modifed Omega only
            omega = np.ones((len(prob),len(prob)))*1e-16
            for i in range(0,len(prob)):
                var = prob[i][1]*(1-prob[i][1])
                omega[i][i] = var
                if len(prob) > 1:
                    for j in range(i,len(prob)-1):
                        omega[j][j+1] = corr[j][1].loc[prob[j+1][0]]
                        omega[j+1][j] = omega[j][j+1]
       
        # Calculate equilibrium excess returns with views incorporated
        sub_a = np.linalg.inv(np.dot(tau, C))
        sub_b = np.dot(np.dot(np.transpose(P), np.linalg.inv(omega)), P)
        # sub_c = np.dot(inv(np.dot(tau, C)), Pi)
        # R is historical return, to substitute of the equilibrium returns Pi
        sub_c = np.dot(np.linalg.inv(np.dot(tau, C)), R.T)
        sub_d = np.dot(np.dot(np.transpose(P), np.linalg.inv(omega)), Q)
        
        A = sub_a + sub_b
        AI = np.linalg.inv(A)
        print(sub_c)
        print(sub_d)
        B = (sub_c + sub_d.reshape(len(sub_d),-1))
        Pi_bl = np.dot(A, B)
        
        
        return Pi_bl
    

               