import pandas as pd
import numpy as np
import datetime as dt
import os
from backtest import Backtester


def price_to_usd(price, char, exchange_data_path):
    ex = char['CURRENCY'].unique()
    ex = np.delete(ex, np.where(ex == 'USD'))
    curex = [pd.read_csv(exchange_data_path + '%sUSD=X.csv' %e, index_col='Date')['Adj Close'].to_frame() for e in ex]
    curex = pd.concat(curex, axis=1)
    curex.columns = ex
    curex.index = pd.to_datetime(curex.index, format='%Y-%m-%d')
    curex = curex.reindex(price.index)
    curex = curex.ffill().bfill()
    price_usd = price.copy()
    for t in price.columns:
        cur = char.loc[t]['CURRENCY']
        if cur != 'USD':
            price_usd[t] *= curex[cur]
    return price_usd


def bond_class(rating):
    mark = grade_to_num('BB+')
    mark_nr = grade_to_num('NR')
    HY = []
    IG = []
    NR = []
    gd = pd.DataFrame()
    gd['RTG_MOODY'] = ['B3','B2','B1','Ba3','Ba2','Ba1','Baa3','Baa2','Baa1',
                       'A3','A2','A1','Aa3','Aa2','Aa1','Aaa']
    gd['RTG_FITCH'] = ['B-','B','B+','BB-','BB','BB+','BBB-','BBB','BBB+',
                       'A-','A','A+','AA-','AA','AA+','AAA']
    gd['RTG_SP'] = ['B-','B','B+','BB-','BB','BB+','BBB-','BBB','BBB+',
                    'A-','A','A+','AA-','AA','AA+','AAA']
    for t in rating.index:
        flag_IG = True
        cnt_NR = 0
        for r,ag in zip(rating.loc[t], rating.loc[t].index):
            n = grade_to_num(r, gd[ag])
            if n <= mark_nr:
                cnt_NR +=1
                continue
            elif n < mark:
                HY.append(t)
                flag_IG = False
                break
        if flag_IG:
            if cnt_NR < rating.shape[1]:
                IG.append(t)
            else:
                NR.append(t)
    cat = pd.Series()
    for t in rating.index:
        if t in HY:
            cat.loc[t] = 'HY'
        elif t in IG:
            cat.loc[t] = 'IG'
        elif t in NR:
            cat.loc[t] = 'NR'
    cat = cat.to_frame()
    cat.columns = ['CAT']
    return cat


def grade_to_num(grade, gs=None):
    if grade in [np.nan]:
        return -10
    if grade.startswith('C'):
        return -1
    if gs is None:
        gs = ['B-','B','B+','BB-','BB','BB+','BBB-','BBB','BBB+',
              'A-','A','A+','AA-','AA','AA+','AAA']
    grade_ = grade.replace(' ','').replace('u','').replace('*+','').replace('*-','').replace('*','')    
    for i,g in enumerate(gs):
        if grade_ == g:
            return i
    # print(grade)
    # only NR WR WD left
    return -10
    

class Performance:
    def __init__(self, start_date, end_date, price, rating, char, holdings, exchange_data_path):
        price = price.loc[start_date:end_date]
        price = price.dropna(how='all', axis=0).dropna(how='all', axis=1)
        price = price.ffill().bfill()
        price.index = pd.to_datetime(price.index, format='%Y-%m-%d')
        bond = list(set(price.columns) & set(rating.index) & set(char.index) & set(holdings.index))
        #self.start_date = start_date
        #self.end_date = end_date
        self.price_usd = price_to_usd(price[bond], char.loc[bond], exchange_data_path)
        self.bond_class = bond_class(rating.loc[bond])
        self.char = char.loc[bond]
        holdings = holdings.loc[bond].T
        holdings.index = pd.to_datetime(holdings.index, format='%Y-%m-%d')
        self.holdings = holdings
        price_rb = self.price_usd.reindex(self.holdings.index)
        self.holding_price = self.holdings * price_rb
        self.return_usd = self.price_usd.pct_change().dropna(how='all')
        self.rb_date = self.holding_price.index
    
    def backtest(self, field, direc):
        if field in ['COUNTRY_FULL_NAME', 'INDUSTRY_SECTOR']:
            name_BC = self.char[field].unique()
            BC = [[b for b in self.char.index 
                   if self.char.loc[b,field]==name] 
                  for name in name_BC]
        else:
            name_BC = ['IG','HY','NR']
            BC = [[b for b in self.bond_class.index 
                   if self.bond_class.loc[b].item()==name] 
                  for name in name_BC]
        annualization = 252
        leverageCap = 1
        lev_cost = 0
        min_level = 0
        BT = Backtester()
        for name, bc in zip(name_BC, BC):
            directory = direc + name + '/'
            try:
                os.mkdir(directory)
            except FileExistsError:
                print('', end='\r')
            composition = self.holding_price[bc]/self.holding_price[bc].values.sum()
            dur = self.char.loc[bc, 'DUR_ADJ_MID'].dropna()
            comp = composition.T.reindex(dur.index)
            comp = comp/comp.sum()
            duration = comp.T.values.dot(dur.values)
            print('duration of %s is %f'%(name, duration))
            BT.backtest(self.return_usd[bc], composition, self.rb_date, annualization, 
                        name, directory, leverageCap, lev_cost, min_level)
            
