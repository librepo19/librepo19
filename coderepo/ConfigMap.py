import configparser
import datetime as dt
config = configparser.ConfigParser()
EQList = \
    [
    '700 HK Equity',    '1813 HK Equity',   '2317 TT Equity',
    '688 HK Equity',    '992 HK Equity',    'CF US Equity',
    '005930 KS Equity', '16 HK Equity',     '20 HK Equity',     
    '83 HK Equity'
    ]
EQStr = ''
for i in EQList:
    EQStr += i + '|'
config['DEFAULT']['equities']       = EQStr
config['DEFAULT']['start_date']     = str(dt.date(2016,1,1))
config['DEFAULT']['rb_period']      = '252'
config['DEFAULT']['rb_interval']    = '21'
config['DEFAULT']['option']         = 'Max Sharpe'
with open('config.ini', 'w') as configfile:
  config.write(configfile)
'''
Reading Config File
config = configparser.ConfigParser()
config.read(file)
config.default_section
>>> 'DEFAULT'
for key in config[config.default_section]:
    print(key)
>>>
equities
start_date
rb_period
rb_interval
option
for key in config[config.default_section]:
    print(config[config.default_section][key])
>>>
700 HK Equity|1813 HK Equity|2317 TT Equity|688 HK Equity|992 HK Equity|CF US Equity|005930 KS Equity|16 HK Equity|20 HK Equity|83 HK Equity|
2015-01-01
252
21
Max Sharpe
'''