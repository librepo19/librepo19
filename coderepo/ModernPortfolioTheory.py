import numpy as np
import pandas as pd
import datetime as dt
import scipy.optimize as sco
import empyrical as empy

class ModernPortfolioTheory(object):

    """description of class"""

    # Need to pass RawData, option
    def MPT(self, cor_ret_list, tickers, tic_na_free, Tbounds, group_pointer, groups, Gbounds, 
            duration, time_horizon, option, leverage=1, lev_cost=0, annualization=252):
        
        risk_free = 0
        As, lb, ub = self.correct_constraint(tickers, tic_na_free, Tbounds, group_pointer, groups, Gbounds, leverage)
        A = np.vstack(As)
        x0_eq, x0s = self.initial_guess(cor_ret_list[0], leverage, annualization, risk_free)
        
        constraint_eq = {'type':'eq', 'fun': lambda x: np.sum(x)-leverage} # weight sum is leverage
        constraint_lb = {'type':'ineq', 'fun': lambda x: A@x - lb}
        constraint_ub = {'type':'ineq', 'fun': lambda x: ub - A@x}
        constraints = [constraint_eq, constraint_lb, constraint_ub]
        
        if time_horizon != None:
            duration = duration[tic_na_free]
            constraint_dur = {'type':'eq', 'fun': lambda x: duration.dot(x)-time_horizon}
            constraints.append(constraint_dur)
        
        if option == 'Min Vol':
            # Function, Volatility
            fun = lambda x: \
                sum([empy.annual_volatility(r.dot(x), annualization=annualization) \
                     for r in cor_ret_list_sel])
            #fun_t = lambda x: empy.annual_volatility(cor_ret_test.dot(x), annualization=annualization)
        elif option == 'Max Sharpe':
            # Function, Sharpe Ratio
            fun = lambda x: \
                sum([-empy.sharpe_ratio(r.dot(x), risk_free, annualization=annualization) \
                     for r in cor_ret_list_sel])
            #fun_t = lambda x: -empy.sharpe_ratio(cor_ret_test.dot(x), risk_free, annualization=annualization)
        else:
            raise ValueError('MPT portType should be: Min Vol or Max Sharpe.')
        
        cor_ret_list_sel = cor_ret_list[:30] # <<<<<<
        
        result = sco.minimize(fun, x0_eq, method='SLSQP', constraints=constraints)
        #print('result')
        #print(result)
        x_opt = self.correct_weight(result.x, leverage)
        if result.success and self.in_range(A@x_opt, lb, ub):
            rerun = False
        else:
            rerun = True
        #print('rerun')
        #print(rerun)
        for i, x0 in enumerate(x0s):
            result_ran = sco.minimize(fun, x0, method='SLSQP', constraints=constraints)
            #print('result_ran')
            #print(result_ran)
            xr_opt = self.correct_weight(result_ran.x, leverage)
            if result_ran.success and self.in_range(A@xr_opt, lb, ub):
                if rerun or result_ran.fun < result.fun:
                    result = result_ran
                    x_opt = xr_opt
                    rerun = False
            #print('rerun')
            #print(rerun)
        
        if rerun:
            print('All initial guesses failed in Optim. Please check if feasible')
            
        """
        xs = []
        Efs = []
        for i, cor_ret in enumerate(cor_ret_list):
            # Computed Results and Weight
            result = sco.minimize(fun, x0_eq, method='SLSQP', constraints=constraints)
            x_opt = self.correct_weight(result.x, leverage)
            if result.success and self.in_range(A@x_opt, lb, ub):
                rerun = False
            else:
                rerun = True
            for i, x0 in enumerate(x0s):
                result_ran = sco.minimize(fun, x0, method='SLSQP', constraints=constraints)
                xr_opt = self.correct_weight(result_ran.x, leverage)
                if not result.success:
                    result = result_ran
                    x_opt = xr_opt
                elif result_ran.success and self.in_range(A@xr_opt, lb, ub) and (result_ran.fun < result.fun):
                    result = result_ran
                    x_opt = xr_opt
                    rerun = False
            '''
            max_iter = 50
            for i in range(max_iter):
                if not rerun:
                    break
                x0 = np.random.random(len(tic_na_free))
                x0 = x0/sum(x0)*leverage
                result_ran = sco.minimize(fun, x0, method='SLSQP', constraints=constraints)
                print('result_ran')
                print(result_ran)
                xr_opt = self.correct_weight(result_ran.x, leverage)
                #xr_opt = result_ran.x
                if result_ran.success and self.in_range(A@xr_opt, lb, ub) and (result_ran.fun < result.fun):
                    result = result_ran
                    x_opt = xr_opt
                    rerun = False
            if rerun:
                print('no solution with in bounds after %d trials'%max_iter)
            '''
            # SLSQP: Sequential Least SQuares Programming, Contraints are required for COBYLA,SLSQP
            # If Objective Function error, please check inputs and that they are aligned properly as well
            xs.append(x_opt)
            fs = []
            for j, cor_ret_test in enumerate(cor_ret_list):
                f = fun_t(x_opt)
                fs.append(f)
            Ef = sum(fs)/len(fs)
            Efs.append(Ef)
        idx = np.nanargmin(Efs)
        x = xs[idx]
        w = pd.Series([0]*len(tickers), index=tickers)
        w[tic_na_free] = x
        f = Efs[idx]
        if option == 'Max Sharpe':
            f *= -1
        
        
        """
        
        
        w = pd.Series([0]*len(tickers), index=tickers)
        w[tic_na_free] = x_opt
        #f = result.fun/len(cor_ret_list)
        
        f = sum([-empy.sharpe_ratio(r.dot(x_opt), risk_free, annualization=annualization) \
                     for r in cor_ret_list])/len(cor_ret_list)
        
        if option == 'Max Sharpe':
            f *= -1
        
        
        return w, f, As, lb, ub
        
    def correct_constraint(self, tickers, tic_na_free, Tbounds, group_pointer, groups, Gbounds, leverage):
        
        nt = len(tic_na_free)
        Tbounds = np.array(Tbounds).reshape(-1,2)
        Tbounds = pd.DataFrame(data=Tbounds, index=tickers).loc[tic_na_free]
                
        As = [np.identity(nt)]
        bs = [Tbounds]
        for ig, d in enumerate(groups):
            ng = len(d)
            GB = np.array(Gbounds[ig]).reshape(-1,2)
            gps = pd.Series(data=group_pointer[ig], index=tickers)
            gps = gps.loc[tic_na_free].values
            Ag = np.zeros((ng, nt))
            for i,g in enumerate(d):
                for j,p in enumerate(gps):
                    if p == g:
                        Ag[i,j] = 1
            # if some group have no assets, ignore that group
            row_sum = Ag.sum(axis=1)
            group_ignore = [i for i, x in enumerate(row_sum) if x == 0]
            Ag1 = np.delete(Ag, group_ignore, axis=0)
            GB = np.delete(GB, group_ignore, axis=0)
            # if some assets are not grouped, put them to the new group
            Ag2 = 1-Ag1.sum(axis=0)
            if Ag2.sum() == 0: # well grouped
                Ag2 = np.zeros((0, nt)) 
            else: # not well grouped
                GB = np.vstack([GB,[0,1]])
            As.append(np.vstack([Ag1,Ag2]))
            bs.append(GB)

        b = np.vstack(bs)
        lb = b[:,0]
        ub = b[:,1]
        
        # correct the bounds from compared with one, to compared with leverage
        lb *= leverage
        ub *= leverage
        
        return As, lb, ub
    
    def initial_guess(self, cor_ret, leverage, annualization, risk_free=0, n_ran = 2):
        '''initial guess on weight'''
        tic = cor_ret.columns
        nt = len(tic)
        
        x0_eq = [1/nt*leverage]*nt
        
        x0_a = np.zeros(nt)
        vol = [empy.annual_volatility(cor_ret[t], annualization=annualization) for t in tic]
        argmin = np.argmin(vol)
        x0_a[argmin] = leverage
            
        x0_b = np.zeros(nt)
        sr = [empy.sharpe_ratio(cor_ret[t], risk_free, annualization=annualization) for t in tic]
        argmax = np.argmax(sr)
        x0_b[argmax] = leverage
            
        x0s = [x0_a, x0_b]
        for i in range(n_ran):
            x = np.random.random(nt)
            x0s.append(x/sum(x)*leverage)
        
        x0s.append(np.zeros(nt))
        
        return x0_eq, x0s
    
    def correct_weight(self, x, S, eps=1.0e-5):
        for i,w in enumerate(x):
            if w < eps:
                x[i] = 0
        x = x/sum(x)*S
        return x
    
    def in_range(self, x, lb, ub, eps=1.0e-5):
        if any(x<lb-eps) or any(x>ub+eps):
            return False
        return True
