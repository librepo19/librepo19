import pandas as pd
#from GeneralUtilities import GeneralUtilities
#GU = GeneralUtilities()

def FM(df_in, ifv_col=None, con_col=None, con_val=None, idx_name=None):
    
    '''
    df_in.index has to be unique
    ifv_col must be None or col_in name of [idx_out, field_out, value_out]
    only select the lines where values in con_col equals to the con_val
    index_col: 
        None     no index colnmns for the input csv
    '''
    #df_in = pd.read_csv(dataPath + '%s.csv' %fileInName, index_col=index_col)
    if not df_in.index.is_unique:
        # df_in.index is not unique
        # create new index which is unique
        df_in['idx_new'] = list(range(0, df_in.shape[0]))
        df_in = df_in.set_index('idx_new')    
    if ifv_col == None:
        ifv_col = df_in.columns
    if idx_name == None:
        idx_name = ifv_col[0]
    df_out = pd.DataFrame([])
    for i in df_in.index:
        if (con_col==None) or (df_in.loc[i,con_col]==con_val):
            [idx, field, value] = [df_in.loc[i,c] for c in ifv_col]
            df_out.loc[idx, field] = value
    df_out = df_out.rename_axis(idx_name)
    #GU.outputFile(dataPath + fileOutName + '.csv', data)    
    return df_out
