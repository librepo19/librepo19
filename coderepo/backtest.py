import numpy as np
import pandas as pd
import datetime as dt
import empyrical as empy
import matplotlib.pyplot as plt
from GeneralUtilities import GeneralUtilities
# General Utilities Object Instance
GU = GeneralUtilities()

class Backtester():
    
    def backtest(self, rawData, composition, rb_date, annualization, portName, directory=None, leverage=1, lev_cost=0, min_level=0):

        line_ret, line_weight = self.cal_return(rawData, composition, rb_date)
        
        line_ret.to_csv(directory + 'lineReturn.csv', header=['line return'])
        line_weight.to_csv(directory + 'lineWeight.csv')
        
        line_lev_ret, cumprods_df = self.sim_leverage(line_ret, rb_date, annualization, leverage, lev_cost, min_level)
        
        line_lev_ret.to_csv(directory + 'lineLeveragedReturn.csv', header=['line leveraged return'])
        cumprods_df.to_csv(directory + 'cumprods.csv', header=['cum prods'])
        
        fig = plt.figure()
        ax = cumprods_df.plot()
        ax.set_title(portName)
        plt.savefig(directory + 'cumprod.png')
        plt.close
        
        report = self.gen_report(line_lev_ret, annualization, portName, False)
        report_annual = self.gen_report(line_lev_ret, annualization, portName)
        
        GU.outputFile(directory + 'backtest_result.csv', report)
        GU.outputFile(directory + 'backtest_result_annual.csv', report_annual)
        
        return report
            
    def cal_return(self, rawData, composition, rb_date):
        line_weight = composition.reindex(rawData.index).ffill().dropna()        
        line_ret = line_weight.copy() # Replication 
        # You need to copy
        # If you merely assign, DataFrame will be interconnected
        #line_weight.to_csv(r'C:\Users\44128862\source\repos\CodeRepo\CodeRepo\lineWeight_RAW.csv')
        rawData_Overall = rawData.reindex(line_weight.index)+1 
        n = composition.shape[1]
        na_warning = False
        previousWeight = line_weight.iloc[0,:].copy() 
        for i, v in line_ret.iloc[1:].iterrows():
            lw = np.zeros(n)
            # lw = previousWeight*rawData_Overall.loc[i] 
            # sum maybe different from L
            # 0*na -> 0
            for j, ro in enumerate(rawData_Overall.loc[i]):
                t = rawData_Overall.columns[j]
                if previousWeight[t] == 0:
                    lw[j] = 0
                elif not np.isnan(ro):
                    lw[j] = previousWeight[t]*ro
                else:
                    na_warning = True
                    lw[j] = previousWeight[t]
            line_ret.loc[i] = lw/sum(previousWeight)
            if i in rb_date:
                # get the re-balanced weights, sum = L
                previousWeight = line_weight.loc[i] # for next day
            else:
                line_weight.loc[i] = lw # update the weights
                previousWeight = line_weight.loc[i] # for next day
            
            '''
            if i in rb_date:
                previousWeight = line_weight.loc[i,:].copy() 
                # get the re-balanced weights, sum = L
            
            # line_weight.loc[i,:] = previousWeight*rawData_Overall.loc[i,:] 
            # update the weights, sum maybe different from L
            for j, ro in enumerate(rawData_Overall.loc[i,:]):
                t = rawData_Overall.columns[j]
                if previousWeight[t] == 0:
                    line_weight.loc[i,t] = 0
                elif not np.isnan(ro):
                    line_weight.loc[i,t] = previousWeight[t]*ro
                else:
                    na_warning = True
                    line_weight.loc[i,t] = previousWeight[t]
            line_ret.loc[i,:] = line_weight.loc[i,:]/sum(previousWeight)
            previousWeight = line_weight.loc[i,:] # for next day
            '''
        
        if na_warning:
            print('Warning: There are still missing data in rawData')
            print('back test assume the missing return to be 0')
        
        line_ret = line_ret.sum(1)-1
        
        return line_ret, line_weight
    
    def sim_leverage(self, line_ret, rb_date, annualization, leverage=1, lev_cost=0, min_level=0):
        '''
        cumprod             : leveraged ratio compared to day 0
                                  previous_rb_cumprod * prod
        
        previous_rb_cumprod : leveraged ratio of previos rb day compared to day 0
                                  PI(1+L(cump-1)) from first rb to previos rb
                                  
        prod                : leveraged ratio compared to previos rb day
                                  1+L(cump-1)
                                  
        cump                : ratio compared to previos rb day
                                  PI(1+line_ret) from previous rb day to that day
        
        line_lev_ret        : [leveraged return compared to previous day]
        
        LCMF                : leverage cost modification factor
        say 1000F + 1000(L-1)F = 1000LF
        where 1000F = 1000 - 1000(L-1)FC
        hence F = 1/(1+(L-1)*C)
        
        LLC                 : leverage cost for borrowing (L-1) for one line (day or month)
        '''
        # print('leverage is %.1f'%leverage)
        # print('annual leverage cost is %.5f'%lev_cost)
        cumprods = []
        cumprod = 1
        line_lev_ret = []
        # LCMF = 1/(1+(leverage-1)*lev_cost)
        LLC = (leverage-1)*lev_cost/annualization
        warning = False
        broken = False
        for i,d in enumerate(line_ret.index):
            if d in rb_date:
                previous_rb_cumprod = cumprod 
                cump = 1+line_ret.loc[d]
                previous_prod = 1
                #lcmf = LCMF
                llc = LLC
            else:
                cump *= (1+line_ret.loc[d])
                previous_prod = prod
                #lcmf = 1
                llc += LLC
            prod = 1 + leverage*(cump-1) - llc
            line_lev_ret.append(prod/previous_prod - 1)
            cumprod = previous_rb_cumprod*prod
            cumprods.append(cumprod)
            if((not warning) and cumprod<min_level):
                print("below %.2f of original level on %s"%(min_level, d.strftime('%Y-%m-%d')))
                warning = True
            if(cumprod<=0):
                print("lost all money on %s"%d.strftime('%Y-%m-%d'))
                broken = True
                cumprods[i] = 0 # adjust -ve prod to zero
                line_lev_ret[i] = -1 # adjust prod<-1 to -1
                brokeIdx = i
                break
        if broken:
            for i in range(brokeIdx+1,len(line_ret.index)):
                cumprods.append(0)
                line_lev_ret.append(0)
        # print('Overall Return is %f'%(cumprods[-1]-1))
        line_lev_ret = pd.Series(line_lev_ret, index=line_ret.index)
        cumprods_df = pd.Series(cumprods, index=line_ret.index)
        
        return line_lev_ret, cumprods_df
    
    def gen_report(self, line_lev_ret, annualization, portName, annual=True):
        
        start_date = line_lev_ret.index[0] # First Element
        end_date = line_lev_ret.index[-1] # Last Element
        
        if annual:
            starts = [dt.date(y,1,1) for y in np.arange(start_date.year, end_date.year + 1)]
            ends = [dt.date(y,12,31) for y in np.arange(start_date.year, end_date.year + 1)]
        else:
            starts = [start_date]
            ends = [end_date]
        details={}
        for i in range(len(starts)):
            
            returns = line_lev_ret[starts[i]:ends[i]]
            # as the actual growth is required, empy.cum_returns()[-1] is used 
            #x = empy.cum_returns(returns)[-1]
            #y = len(returns) / np.sqrt(annualization) * returns.std()
            #s = x/y
            
            sd = returns.index[0]
            ed = returns.index[-1]
            str = '%s/%2s/%2s to %2s/%2s'%(sd.year, sd.month, sd.day, ed.month, ed.day)
            
            details.update({str: [empy.annual_return(returns, annualization=annualization), 
                                  empy.annual_volatility(returns, annualization=annualization), 
                                  empy.sharpe_ratio(returns, annualization=annualization), 
                                  empy.cum_returns(returns).iloc[-1],
                                  #x, #y, #s, 
                                  empy.max_drawdown(line_lev_ret[starts[i]:ends[i]])]})
        details = pd.DataFrame(details).T
        details.columns=['Annual Return', 'Annual Volatility', 'Annual Sharpe Ratio', 
                         #'Actual Return', 'Actual Volatility', 'Actual Sharpe Ratio', 'Max. DD']
                         'Cumulative Return', 'Max. DD']
        
        result = []
        result.append(details)
        names = []
        names.append(portName)
        
        if annual:
            year = result[-1].index
            result = pd.concat(result)
            result.index = pd.MultiIndex.from_product([names, year])
        else:
            result = pd.concat(result)
            result.index = names
        
        dd_ratio = result['Cumulative Return']/result['Max. DD']
        dd_ratio.name = 'Return DD. Ratio'
        result = pd.concat([result, dd_ratio], axis=1)
        # print(result)
        
        return result
