import itertools
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import yaml
from GeneralUtilities import GeneralUtilities
from PortfolioAnalyser_abcde import PortfolioAnalyser_abcde
from ModernPortfolioTheory import ModernPortfolioTheory
from backtest import Backtester

configfile = r'config.ini'
configfile_excel = r'config_13etf.xlsx'
configfile_yaml = r'config.yaml'

# General Utilities Object Instance
GU = GeneralUtilities()

# Portfolio Analyser Object Instance
PA = PortfolioAnalyser_abcde()

# Backtester Object Instance
BT = Backtester()

# Initial Config Object
iConfig = GU.importConfig(configfile)

def loadConfig(fileName):
    # Allows for different ways of config loading
    if fileName.split('.')[-1] == 'ini':
        iConfig = GU.importConfig(fileName)
        tickers = []
        # Loading Equities List
        for i in iConfig['equities'].split('|'):
            tickers.append(i)
        tickers = list(filter(None,tickers))
        # print(tickers)
        # rb Parameters
        rb_period = int(iConfig['period'])
        rb_interval = int(iConfig['interval'])
        # Port Type
        portType = iConfig['porttype']
        # Option
        option = iConfig['option']
        # Data Path
        dataPath = iConfig['data_path']
        # Start Date
        startDate = iConfig['start_date']

    elif fileName.split('.')[-1] == 'xlsx':
        tConfig = pd.read_excel(fileName,'Tickers')
        g1Config = pd.read_excel(fileName,'Groups1')
        g2Config = pd.read_excel(fileName,'Groups2')
        g3Config = pd.read_excel(fileName,'Groups3')
        lConfig = pd.read_excel(fileName,'Leverage',index_col='Parameter')
        iConfig = pd.read_excel(fileName,'InitialConfig',index_col='Parameter')
        
        tickers = tConfig['Ticker'].tolist()
        LB = tConfig['Lower Bound'].tolist()
        if sum(LB) > 1:
            raise ValueError('sum of LB must be <= 1')
        UB = tConfig['Upper Bound'].tolist()
        if sum(UB) < 1:
            raise ValueError('sum of UB must be >= 1')
        Tbounds = []
        for i,v in enumerate(LB):
            Tbounds.append([LB[i],UB[i]])
        group_pointer1 = tConfig['Group1'].tolist()
        group_pointer2 = tConfig['Group2'].tolist()
        group_pointer3 = tConfig['Group3'].tolist()
        #target_return = tConfig['Target Return (monthly)'].tolist()
        #if all(np.isnan(target_return)):
        #    target_return = None
        #else:
        #    target_return = pd.Series(target_return, index=tickers)
        try:
            duration = tConfig['duration'].tolist()
        except:
            duration = None
        
        groups1 = g1Config['Group'].tolist()
        GB1 = g1Config['Group Bound'].tolist()
        Gbounds1 = []
        for i,v in enumerate(GB1):
            Gbounds1.append([0,v])
        
        groups2 = g2Config['Group'].tolist()
        GB2 = g2Config['Group Bound'].tolist()
        Gbounds2 = []
        for i,v in enumerate(GB2):
            Gbounds2.append([0,v])
        
        groups3 = g3Config['Group'].tolist()
        GB3 = g3Config['Group Bound'].tolist()
        Gbounds3 = []
        for i,v in enumerate(GB3):
            Gbounds3.append([0,v])
        
        leverageCap = lConfig.loc['Max Leverage']['Value']
        try:
            leverageCap = int(leverageCap)
        except:
            leverageCap = 1
        lev_cost = lConfig.loc['Leverage Cost']['Value']
        if np.isnan(lev_cost):
            lev_cost = 0
        min_level = lConfig.loc['Min Level']['Value']
        if np.isnan(min_level) or min_level>=1 or min_level<0:
            min_level = 0
        
        dataFreq = iConfig.loc['Data Freq']['Value']
        if dataFreq == 'daily':
            annualization = 252
        elif dataFreq == 'weekly':
            annualization = 52
        elif dataFreq == 'monthly':
            annualization = 12
        else:
            raise ValueError('dataFreq must be [daily, weekly, monthly]')
        rb_interval = iConfig.loc['Rebalance Interval (line/rb)']['Value']
        rb_period = iConfig.loc['Rebalance Period (line/rb)']['Value']
        inputType = iConfig.loc['Input Type']['Value']
        if inputType not in ['price', 'return']:
            raise ValueError('inputType must be [price, return]')
        startDate = iConfig.loc['Start Date']['Value'].date()
        try:
            endDate = iConfig.loc['End Date']['Value'].date()
        except AttributeError:
            endDate = None
        
        tp_fn = iConfig.loc['Target Price File Name']['Value']
        black_litterman = iConfig.loc['Black Litterman']['Value']
        if black_litterman in [False, 'False','false', np.nan]:
            black_litterman = False
        else:
            black_litterman = True
        if black_litterman and endDate == None and tp_fn == None:
            raise ValueError('BL require End Date (at least one rb interval before EOF)')
        view = iConfig.loc['View (absolute/relative)']['Value']
        try:
            time_horizon = iConfig.loc['Time Horizon']['Value']
            if time_horizon in [np.nan, 'None']:
                time_horizon = None
        except:
            time_horizon = None
        portType = iConfig.loc['Port Type']['Value']
        option = iConfig.loc['Option']['Value']
        
        dataPath = iConfig.loc['Data Path']['Value']
        dataFileName = iConfig.loc['Data File Name']['Value'] # filename or none(separate files)
        dateFormat = iConfig.loc['Date Format']['Value'] # 'i.e. %d-%m-%y'
        if dateFormat in [np.nan]:
            dateFormat = '%d-%m-%y'
        fill_method = iConfig.loc['Fill Method']['Value']
        directory = iConfig.loc['Output Directory']['Value']
        run_test = iConfig.loc['Run Test']['Value'] # True or False
        if run_test in [False, 'False','false', np.nan]:
            run_test = False
        else:
            run_test = True
        plot_graph = iConfig.loc['Plot Graph']['Value'] # all or last or else plot nothing
        num_portfolios = iConfig.loc['Num Portfolios']['Value']
        
        group_pointer = [group_pointer1, group_pointer2, group_pointer3]
        groups = [groups1, groups2, groups3]
        Gbounds = [Gbounds1, Gbounds2, Gbounds3]
    
    elif fileName.split('.')[-1] == 'yaml':
        with open(fileName, 'r', encoding="utf-8") as file:
            #data = yaml.load(file, Loader=yaml.FullLoader) # for YAML (latetr ver)
            data = yaml.load(file) # for YAML 3.12
        tickers = data['tickers']
        Tbounds = data['Tbounds']
        group_pointer = data['group_pointer']
        groups = data['groups']
        Gbounds = data['Gbounds']
        duration = data['duration']
        startDate = data['start_date']
        endDate = data['end_date']
        time_horizon = data['time_horizon']
        black_litterman = data['black_litterman']
        # target_return = data['target_return']
        view = data['view']
        tp_fn = data['tp_fn']
        leverageCap = data['leverageCap']
        lev_cost = data['lev_cost']
        min_level = data['min_level']
        annualization = data['annualization']
        rb_interval = data['rb_interval'] 
        rb_period = data['rb_period'] 
        inputType = data['inputType'] 
        portType = data['portType'] 
        option = data['option'] 
        dataPath = data['dataPath'] 
        dataFileName = data['dataFileName'] 
        dateFormat = data['dateFormat']
        fill_method = data['fill_method']
        directory = data['directory']
        run_test = data['run_test']
        plot_graph = data['plot_graph']
        num_portfolios = data['num_portfolios']
        
    # quick config
    directory = './result/13etf_prob01_ran30/'
    return tickers, Tbounds, group_pointer, groups, Gbounds, \
                startDate, endDate, duration, time_horizon, \
                black_litterman, view, tp_fn, \
                leverageCap, lev_cost, min_level, \
                annualization, rb_interval, rb_period, inputType, portType, option, \
                dataPath, dataFileName, dateFormat, fill_method, \
                directory, run_test, plot_graph, num_portfolios

# Loading Initial Configuration
tickers, Tbounds, group_pointer, groups, Gbounds, \
    start_date, end_date, duration, time_horizon, \
    black_litterman, view, tp_fn, \
    leverageCap, lev_cost, min_level, \
    annualization, rb_interval, rb_period, inputType, portType, option, \
    dataPath, dataFileName, dateFormat, fill_method, \
    directory, run_test, plot_graph, num_portfolios = loadConfig(configfile_excel)

try:
    os.mkdir(directory)
except FileExistsError:
    # print('directory made')
    print('', end='\r')

configlog = [tickers, Tbounds, group_pointer, groups, Gbounds, 
             start_date, end_date, duration, time_horizon, 
             black_litterman, view, tp_fn, 
             leverageCap, lev_cost, min_level, 
             annualization, rb_interval, rb_period, inputType, portType, option, 
             dataPath, dataFileName, dateFormat, fill_method, 
             directory, run_test, plot_graph, num_portfolios]

GU.outputFile(directory + 'configlog.csv', configlog)

portName, rawData, rb_date, composition, func, figs_opt, tic_dropped, comp_pass \
    = PA.Portfolio(tickers, Tbounds, group_pointer, groups, Gbounds, 
                   start_date, end_date, duration, time_horizon, 
                   black_litterman, view, tp_fn, 
                   leverageCap, lev_cost, 
                   annualization, rb_interval, rb_period, inputType, portType, option, 
                   dataPath, dataFileName, dateFormat, fill_method, 
                   directory, run_test, plot_graph, num_portfolios)

# Create empty parameters 
# May need to be replaced
result = []
y_result = []
names = []

# Data List
rb_date_list = []
annual_return_list = []
annual_volatility_list = []

# Annuals
for d in rb_date:
    annual_return, annual_volatility = PA.opt_result(rawData, composition, d, rb_period, annualization)
    rb_date_list.append(d)
    annual_return_list.append(annual_return)
    annual_volatility_list.append(annual_volatility)
result_new = {'annual_return': annual_return_list, 'annual_volatility': annual_volatility_list}

# Format as DataFrame
result_dataframe = pd.DataFrame(data=result_new, index=rb_date_list)
# Write to Excel
GU.outputFile(directory + 'result_dataframe.csv', result_dataframe)

BT.backtest(rawData, composition, rb_date, annualization, portName, directory, leverageCap, lev_cost, min_level)
