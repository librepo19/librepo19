import configparser
import datetime as dt
import pandas as pd
import empyrical as empy
import matplotlib.pyplot as plt
import csv
class GeneralUtilities(object):
    """description of class"""
    # Group of helpful functions
    # Created by Tay Wee Leong, HSBC Investment Labs
    def sliceData(self, data, index, period):   
        # Data must be a DataFrame
        if isinstance(data,pd.DataFrame):
            # Slicing selected DataFrame
            slicedDF = data.iloc[data.index.get_loc(index) - period:data.index.get_loc(index)] 
	
            # Return object
            return slicedDF
        else:
            print ('Data is not in DataFrame format.')
            return None
    def outputDefaultConfig(self,dataDict, path = None):
        config =  configparser.ConfigParser()
        
        if isinstance(dataDict,dict):
            for i in dataDict:
                # Checks if it is a list
                _str = '' # Reset string
                
                if isinstance(dataDict[i],list):
                    # For each value within list
                    for ii in dataDict[i]:
                        _str += ii + '|'
                else:
                    _str += str(dataDict[i])
                config['DEFAULT'][i]=_str
            filePath = self.pathRectify('config,ini',path)
            with open(filePath, 'w') as configfile:
                config.write(configfile)
        else:
            print('Object is not a dict type.')
            return None
    
    def importConfig(self,filepath , section = 'DEFAULT'):
        config =  configparser.ConfigParser()
        config.read(filepath)
        return config[section]
    def pathRectify(self,file,path = None):
            if path != None:
                return path + file
            else:
                return file
    def outputFile(self,fileName,object):
        if isinstance(object,pd.DataFrame):
            # DataFrame Output
            object.to_csv(fileName)
        elif isinstance(object,list):
            # Stringfy it 
            object = [str(i) for i in object]
            # Output as file
            with open(fileName,'w') as f:
                wr = csv.writer(f,quoting = csv.QUOTE_ALL)
                wr.writerows([i] for i in object) 
                # By default, csv writes reads as list, so if you pass string, it will break up all
    
    def getAssetMetrics(self, rb_date, rb_period):
        return_df = self.sliceData(self.rawData, rb_date, rb_period)
        print(return_df)
        x = empy.annual_return(return_df)
        y = empy.annual_volatility(return_df)
        metric = pd.DataFrame(x)
        metric.columns = ['Returns']
        metric['Volatility'] = y
        #metric.to_csv('metric' + str(rb_date)[:10] + '.csv')
        return metric