import itertools
import datetime as dt
import pandas as pd
import numpy as np
import os
from GeneralUtilities import GeneralUtilities
from PortfolioAnalyser import PortfolioAnalyser
from ModernPortfolioTheory import ModernPortfolioTheory
from backtest import Backtester
directory = 'C:\\Stuff\\repo\\librepo19\\coderepo\\'
configfile = directory + r'config.ini'
#configfile_excel = r'config.xlsx'
configfile_excel = directory + r'config.xlsx'
#configfile_excel = r'PortfolioAnalyser.xlsm'
# General Utilities Object Instance
GU = GeneralUtilities()
# Portfolio Analyser Object Instance
PA = PortfolioAnalyser()
# Backtester Object Instance
BT = Backtester()
# Initial Config Object
iConfig = GU.importConfig(configfile)
def loadConfig(fileName):
    # Allows for different ways of config loading
    if fileName.split('.')[-1] == 'ini':
        iConfig = GU.importConfig(fileName)
        tickers =[]
        # Loading Equities List
        for i in iConfig['equities'].split('|'):
            tickers.append(i)
        tickers = list(filter(None,tickers))
        print(tickers)
        # rb Parameters
        rb_period = int(iConfig['period'])
        rb_interval = int(iConfig['interval'])
        # Port Type
        portType = iConfig['porttype']
        # Option
        option = iConfig['option']
        # Data Path
        dataPath = iConfig['data_path']
    
        # Start Date
        startDate = iConfig['start_date']
		
    elif fileName.split('.')[-1] == 'xlsx':
        iConfig = pd.read_excel(fileName,'InitialConfig',index_col='Parameter')
        # Eg. iConfig.loc['Period']['Value']
        # >>> 252
        tConfig = pd.read_excel(fileName,'Tickers')
        tickers = tConfig['Ticker'].tolist()
        LBound = tConfig['Lbound'].tolist()
        UBound = tConfig['Ubound'].tolist()
        bounds = []
        for i,v in enumerate(LBound):
            bounds.append([LBound[i],UBound[i]])
        cap = tConfig['Cap'].tolist()
        rb_period = iConfig.loc['Period']['Value']
        rb_interval = iConfig.loc['Interval']['Value']
        portType = iConfig.loc['PortType']['Value']
        option = iConfig.loc['Option']['Value']
        dataPath = iConfig.loc['Data Path']['Value']
        startDate = iConfig.loc['Start Date']['Value']
    return tickers,rb_period,rb_interval,portType,option,dataPath,startDate
# Loading Initial Configuration
tickers,rb_period,rb_interval,portType,option,dataPath,startDate = loadConfig(configfile_excel)
# Start Date
# start_date = dt.date(2019,2,1)
start_date = startDate.date()
# Create empty parameters 
# May need to be replaced
result=[]
y_result = []
cumprod = []
names=[]
mean_lvg = []
max_lvg = []
m_mean_lvg = []
m_max_lvg = []
portName, composition, rawData, rb_date = PA.Portfolio(tickers,start_date,rb_interval,portType,option,dataPath = dataPath)
start_idx = rawData.index.get_loc(start_date, method='bfill')
end_idx = rawData.shape[0]
rb_date = rawData.index[np.arange(start_idx,end_idx, rb_interval)].tolist()
for d in rb_date:
    print(d)
    PA.visualize(rawData, composition, d, rb_period, d, str(d)[:10], option=option)
# Structuring Dates
# Start, End, Date Intervals
#start_idx = port.rawData.index.get_loc(start_date, method='bfill') # Accessing properties with port object
#end_idx = port.rawData.shape[0]
#rb_date = port.rawData.index[np.arange(start_idx,end_idx, rb_interval)].tolist()
#GU.outputFile(directory + '%s_Composition.csv' %portName,composition)
GU.outputFile(directory + 'Composition.csv', composition)
# Data List
rb_date_list =[]
annual_return_list = []
annual_volatility_list = []
# Annuals
for d in rb_date:
    annual_return, annual_volatility = PA.opt_result(rawData, composition, d, rb_period)
    rb_date_list.append(d)
    annual_return_list.append(annual_return)
    annual_volatility_list.append(annual_volatility)
result_new = {'annual_return':annual_return_list, 'annual_volatility': annual_volatility_list}
# Format as DataFrame
result_dataframe = pd.DataFrame(data=result_new, index=rb_date_list)
# Write to Excel
GU.outputFile(directory + 'result_dataframe.csv',result_dataframe)
# Mean
mean_lvg.append(composition.sum(1).mean())
# Max
max_lvg.append(composition.sum(1).max())
    
m_mean_lvg.append(composition.sum(1).groupby(by=lambda x: x.year).mean())
m_max_lvg.append(composition.sum(1).groupby(by=lambda x: x.year).max())
r1 = PA.backtest(rawData, composition, rb_date, rb_interval, False)
r2 = PA.backtest(rawData, composition, rb_date, rb_interval)
result.append(r1[0])
y_result.append(r2[0])
cumprod.append(r1[1])
names.append(portName)
result = pd.concat(result)
result.index=names
year = y_result[-1].index
y_result = pd.concat(y_result)
y_result.index = pd.MultiIndex.from_product([names, year])
cumprod = pd.concat(cumprod, axis=1)
cumprod.columns = names
cumprod = cumprod.iloc[-1] # -1 is last element
cumprod.name = 'Cum. Return'
dd_ratio = result['Return']/result['Max. DD']
dd_ratio.name = 'Return DD. Ratio'
mean_lvg = pd.Series(mean_lvg, index=names)
mean_lvg.name= 'Mean Lvg'
max_lvg = pd.Series(max_lvg, index=names)
max_lvg.name = 'Max Lvg'
result = pd.concat([result, cumprod, dd_ratio, mean_lvg, max_lvg], axis=1)
GU.outputFile(directory + 'backtest_result.csv',result)
y_dd_ratio = y_result['Return']/y_result['Max. DD']
y_dd_ratio.name = 'Return DD. Ratio'
y_mean_lvg = pd.concat(m_mean_lvg, axis=0)
y_mean_lvg = y_mean_lvg[y_mean_lvg.T != 0]
y_mean_lvg.index = y_result.index
y_mean_lvg.name = 'Mean Lvg'
y_max_lvg = pd.concat(m_max_lvg, axis=0)
y_max_lvg = y_max_lvg[y_max_lvg.T != 0]
y_max_lvg.index = y_result.index
y_max_lvg.name = 'Max Lvg'
y_result = pd.concat([y_result, y_dd_ratio, y_mean_lvg, y_max_lvg],axis=1)
GU.outputFile(directory + 'yearly_backtest_result.csv', y_result)