import pandas as pd
import numpy as np
import scipy.stats as ss
from sklearn import preprocessing

class LeastCorrelatedSubset(object):
    '''
    
    '''
    def transpose_corr(self, df, feature_select=True, cut_off_corr=0.7):
        df = df.dropna()
        df_m = df[df.applymap(self._isnumber)].dropna(how='all', axis=1)
        df_m = df_m.applymap(float)
        measurable_var = df_m.columns
        categorical_var = [c for c in df.columns if c not in measurable_var]
        df_c = df[categorical_var]
        df = pd.concat([df_m, df_c], axis=1)
        if feature_select:
            corr_pi = self.corr_mixtype(df, categorical_var, measurable_var)
            index_reorder, max_corr, _ = self.LCS(corr_pi)
            sel_c = []
            sel_m = []
            for i,c in enumerate(max_corr):
                if c<=cut_off_corr:
                    if index_reorder[i] in categorical_var:
                        sel_c.append(index_reorder[i])
                    else:
                        sel_m.append(index_reorder[i])
                else:
                    break
            sel = index_reorder[:i]
        else:
            sel = df.columns
            sel_c = categorical_var
            sel_m = measurable_var
        var = []
        for v in sel:
            if v in sel_c:
                dummy = pd.get_dummies(df[v], drop_first=True)
                var.append(dummy)
            else:
                var.append(df[v])
        df_d = pd.concat(var, axis=1)
        min_max_scaler = preprocessing.MinMaxScaler()
        M = min_max_scaler.fit_transform(df_d)
        df_t = pd.DataFrame(M.T, index=df_d.columns, columns=df_d.index)
        return df_t.corr()
        
    def corr_mixtype(self, df, categorical_var, measurable_var):
        cor = pd.DataFrame()
        for i, cm1 in enumerate(df.columns):
            for j, cm2 in enumerate(df.columns):
                if cm1 in categorical_var and cm2 in categorical_var:
                    r = self.cramers_v(df[cm1], df[cm2])
                    cor.loc[cm1, cm2] = r
                elif cm1 in measurable_var and cm2 in measurable_var:
                    r, _ = ss.pearsonr(df[cm1], df[cm2])
                    cor.loc[cm1, cm2] = abs(r)
                elif cm1 in categorical_var and cm2 in measurable_var:
                    r = self.correlation_ratio(df[cm1], df[cm2])
                    cor.loc[cm1, cm2] = r
                else:
                    r = self.correlation_ratio(df[cm2], df[cm1])
                    cor.loc[cm1, cm2] = r
        return cor
    
    def cramers_v(self, x, y):
        confusion_matrix = pd.crosstab(x,y.values)
        chi2 = ss.chi2_contingency(confusion_matrix)[0]
        n = confusion_matrix.sum().sum()
        phi2 = chi2/n
        r,k = confusion_matrix.shape
        phi2corr = max(0, phi2-((k-1)*(r-1))/(n-1))
        rcorr = r-((r-1)**2)/(n-1)
        kcorr = k-((k-1)**2)/(n-1)
        return np.sqrt(phi2corr/min((kcorr-1),(rcorr-1)))
    
    def correlation_ratio(self, categories, measurements):
        fcat, _ = pd.factorize(categories)
        cat_num = np.max(fcat)+1
        y_avg_array = np.zeros(cat_num)
        n_array = np.zeros(cat_num)
        for i in range(0,cat_num):
            cat_measures = measurements[np.argwhere(fcat == i).flatten()]
            n_array[i] = len(cat_measures)
            y_avg_array[i] = np.average(cat_measures)
        y_total_avg = np.sum(np.multiply(y_avg_array,n_array))/np.sum(n_array)
        numerator = np.sum(np.multiply(n_array,
            np.power(np.subtract(y_avg_array,y_total_avg),2)))
        denominator = np.sum(np.power(np.subtract(measurements,y_total_avg),2))
        if numerator == 0:
            eta = 0.0
        else:
            eta = np.sqrt(numerator/denominator)
        return eta

    def LCS(self, corr, verbose=False):
        '''
        finding the max off-diagonal correlation (say at index x,y)
        drop one of index x,y which has higher correlation sum with the index left
        until all index are dropped
        reverse the order of dropped index
        
        corr : the correlation matrix (DataFrame)
        '''
        Q = abs(corr.iloc[:,:])
        n = Q.shape[0]
        cor_drop = []
        t_drop = []
        for i in range(n):
            if verbose:
                print(i,end='\r')
            if i == n-2:
                cor_drop.append(Q.iloc[1,0])
                t_drop.append(Q.index[1])
                cor_drop.append(0)
                t_drop.append(Q.index[0])
                break
            cs = -1
            for j ,tj in enumerate(Q.index):
                for k ,tk in enumerate(Q.index[:j]):
                    if Q.iloc[j,k] > cs:
                        cs = Q.iloc[j,k]
                        ts = [tj,tk]
            cor_drop.append(cs)
            s1 = sum([Q.loc[ts[0],t] for t in Q.index])
            s2 = sum([Q.loc[ts[1],t] for t in Q.index])
            if s1 > s2:
                t = ts[0]
            else:
                t = ts[1]
            t_drop.append(t)
            Q = Q.drop(t, axis=0)
            Q = Q.drop(t, axis=1)
        if verbose:
            print('complete')
        index_reorder = t_drop[::-1]
        max_corr = cor_drop[::-1]
        corr_reorder = corr.loc[index_reorder,index_reorder]
        return index_reorder, max_corr, corr_reorder
    
    def _isnumber(self, x):
        try:
            float(x)
            return True
        except:
            return False



        