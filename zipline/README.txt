0. Change weights to order
C_from_ratios_to_order
composition ---> Orderbook

1. Get orders from trade excel
gen price and order
backtest.csv ---> price2.csv
Portfolio Rebalancing (PFRB).xlsx Orderbook ---> order_original.csv

2. Shift order date
shift
order_original.csv ---> order_shift_one_trading_day.csv 
(using backtest.csv)

3. Run Zipline
Run buy_and_hold.py to generate result.csv and toPyfolio.pickle
If JSON.DECODER error occurs, then see the solution from https://www.itbook5.com/2019/09/11611/
price2.csv, order_shift_one_trading_day.csv ---> result.csv

4. Run Pyfolio
Input toPyfolio.pickle
pyfolio.ipynb
Input file: toPyfolio.pickle



to produce similar result as in backtest.py, just use the unshifted order



stage1
A - use pyportfolio’s black litterman, and the “target-price” as the known price in the future, run this against pyportfolio’s mpt
B - a pure pyportfolio mpt without black-litterman
run A and B against zipline as backtest (please ask Ho Tsang how to run zipline backtest)
compare A and B backtest result, see if A is better

stage2
same as stage1, except using mpt and backtest in the code you’ve been working on
compare result of stage1 and stage2

stage3
same as stage1, but this time use “relative view”, also using god’s view (as in you know the future)

stage4
same as stage3, but using sammi’s prediction of 1/0 as the relative view (TBC)

stage5
same as stage3, but using mpt and backtest of the codes you’ve been working on

