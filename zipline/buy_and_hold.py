#!/usr/bin/env python
#
# Copyright 2015 Quantopian, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from zipline.api import get_datetime,order, record, symbol, set_benchmark,set_commission
from zipline.finance import commission, slippage
from pandas import Timestamp
import pandas as pd
from collections import OrderedDict
import pytz
import os
from zipline.data.bundles import load
from datetime import datetime
import zipline
import matplotlib.pyplot as plt
from matplotlib import style
import pyfolio as pf  
import pytz  
import pickle

def initialize(context):
    stocks_list= ['QQQ','LQD','TLT','DBC','VYM','EWJ','EEM','VGK','TIP','GLD','VTI','IYR','SPY']
    context.has_ordered = False
    context.stocks = stocks_list

def handle_data(context, data):
    """
    if not context.has_ordered:
        for stock in context.stocks:
            order_target_percent(symbol(stock), 0.2)
        context.has_ordered = True
    """
    today = get_datetime().strftime("%Y-%m-%d")
    order = orderFile[orderFile["date"] == today]
    for index, item in order.iterrows():
      name = item["asset"]
      unit = int(item["unit"])
      zipline.api.order(zipline.api.symbol(name), unit)


# Preparing price data 
data = OrderedDict()
csvFile = pd.read_csv("../coderepo/Data/adjclose/backtest_13ETF_fm.csv", index_col=0, parse_dates=['Date'])
for i in range(len(csvFile.columns)):
  column_label = csvFile.columns[i]
  tempColumn = csvFile[[column_label]]
  tempColumn.columns = ['close']
  data[column_label] = tempColumn   

panel = pd.Panel(data)
panel.minor_axis = ['close']
panel.major_axis = panel.major_axis.tz_localize(pytz.utc)

# Preparing list of orders
# orderFile = pd.read_csv("order_original.csv", header=0)
orderFile = pd.read_csv("order_shift_one_trading_day.csv", header=0)
# orderFile["date"] = orderFile["date"].apply(lambda x:x[:x.find(" ")])

perf = zipline.run_algorithm(start=datetime(2017, 12, 29, 0, 0, 0, 0, pytz.utc),
                      end=datetime(2020, 1, 1, 0, 0, 0, 0, pytz.utc),
                      initialize=initialize,
                      capital_base=1000000000,
                      handle_data=handle_data,
                      data=panel)

output = open('toPyfolio.pickle', 'wb')
pickle.dump(perf,output)
perf.to_csv("result.csv")
